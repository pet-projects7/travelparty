require 'open-uri'

class FillCityCovers < ActiveRecord::Migration[5.2]
  def up
    # download_images
    attach_covers
  end

  def down
  end

  private

  def download_images
    Destination.distinct(:city_to).find_each do |destination|
      english_title = destination
                        .city_to[:additional_data]
                        .dig('name_translations', 'en')

      file_name = english_title.gsub(',', '').gsub(' ', '_').gsub('/', '_')

      File.open("#{  Rails.root.to_s }/db/data/city_covers/#{ file_name }.jpg", "wb") do |f|
        f.write HTTParty.get("https://source.unsplash.com/featured/?#{ english_title },city").parsed_response
      end
    end
  end

  def attach_covers
    Destination.distinct(:city_to).find_each do |destination|

      english_title = destination
                        .city_to[:additional_data]
                        .dig('name_translations', 'en')

      file_name = english_title.gsub(',', '').gsub(' ', '_').gsub('/', '_')
      file_path = "db/data/city_covers/#{ file_name }.jpg"

      if File.exist? file_path
        destination
          .city_to
          .covers
          .attach(io: File.open(file_path),
                            filename: "#{ file_name }.jpg",
                            content_type: "image/jpg")
      end

    end
  end
end
