class FillBookingApiInCities < ActiveRecord::Migration[5.2]
  def up
    Settings.europe_arriving_cities.each do |settings_city|
      city = City.find_by(title: settings_city[0])

      if city.present?
        city.update(booking_api: settings_city[1].booking_api)
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
