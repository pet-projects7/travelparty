class AddCountryCodeFromAdditionalData < ActiveRecord::Migration[5.2]
  def up
    Country.where(code: nil).each do |country|
      country.update(code: country.additional_data['code'])
    end
  end

  def down

  end
end
