class FillCountryFlags < ActiveRecord::Migration[5.2]
  def up
    Country.find_each do |country|
      next if country[:additional_data].nil?

      english_title = country[:additional_data].dig('name_translations', 'en').titleize

      file_path = "db/data/flags/#{ english_title }.png"

      if File.exist? file_path
        country.flag.attach(io: File.open(file_path),
                       filename: "#{ english_title }.png",
                       content_type: "image/png")
      end
    end
  end

  def down
  end
end
