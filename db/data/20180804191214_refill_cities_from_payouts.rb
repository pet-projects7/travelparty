# frozen_string_literal: true

# TODO : Возможно сработает не с первого раза, можно зайти на сервер
# и прогнать вручную:
# RAILS_ENV=production rake data:migrate VERSION=20180804191214_refill_cities_from_payouts

class RefillCitiesFromPayouts < ActiveRecord::Migration[5.2]
  def up
    Flight.destroy_all
    HotelReservation.destroy_all
    Opportunity.destroy_all
    Country.destroy_all
    City.destroy_all

    fill_countries
    fill_cities
  end

  def down
    # raise ActiveRecord::IrreversibleMigration
  end

  private

  def fill_countries
    countries = JSON.parse(File.read('db/migrate/countries.json'))
    countries.each do |country|
      Country.create title: country['name'], additional_data: country
    end
  end

  def fill_cities
    cities = JSON.parse(File.read('db/migrate/cities.json'))
    cities.each do |city|
      City.create title: city['name'], code: city['code'], country_code: city['country_code'], additional_data: city
    end
  end
end
