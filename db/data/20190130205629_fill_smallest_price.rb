class FillSmallestPrice < ActiveRecord::Migration[5.2]
  def up
    Destination.find_each do |route|
      flight = Flight
        .order(cost: :asc)
        .where(currency: :rub, city_from_id: route.city_from_id, city_to_id: route.city_to_id)
        .first


      flight.update(smallest_price: true) if flight.present?
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
