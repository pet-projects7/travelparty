# frozen_string_literal: true

class AddDefaultUser < ActiveRecord::Migration[5.2]
  def up
    u = User.create email: 'admin@admin.ru', password: 'password'
    u.add_role :admin
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
