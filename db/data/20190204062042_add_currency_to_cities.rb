class AddCurrencyToCities < ActiveRecord::Migration[5.2]
  def up
    City.where(title: ['Москва', 'Санкт-Петербург']).update(currency: :RUB)
    City.where(title: ['Нью-Йорк']).update(currency: :USD)
    City.where(title: ['Минск']).update(currency: :BYN)
    City.where(title: ['Киев']).update(currency: :UAH)
    City.where(title: ['Берлин']).update(currency: :EUR)
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
