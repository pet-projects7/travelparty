class ReattacheImages < ActiveRecord::Migration[5.2]
  require_relative '20190317091634_fill_country_flags.rb'
  require_relative '20190317094639_fill_city_covers.rb'

  def up
    FillCountryFlags.up
    FillCityCovers.up
  end

  def down; end
end
