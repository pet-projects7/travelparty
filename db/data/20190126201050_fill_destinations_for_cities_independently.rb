class FillDestinationsForCitiesIndependently < ActiveRecord::Migration[5.2]
  def up
    Destination.delete_all

    City.where(title: ['Санкт-Петербург', 'Москва']).pluck(:id).each do |home_city_id|
      creation_hash = City.enabled_ariving_cities.pluck(:id).map {|visiting_city_id| { city_to_id: visiting_city_id, city_from_id: home_city_id } }
      Destination.import creation_hash, validate: false
    end
  end

  def down
    # raise ActiveRecord::IrreversibleMigration
  end
end
