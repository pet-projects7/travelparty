class FillNewDestinationsWithBooking < ActiveRecord::Migration[5.2]
  def up
    Destination.includes(:city_to).each do |destination|
      booking_id = Settings.europe_arriving_cities[destination.city_to.title.to_sym]&.booking_api

      if booking_id.present?
        destination.update(booking_id: booking_id)
      end
    end
  end
end
