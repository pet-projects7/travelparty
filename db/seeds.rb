# frozen_string_literal: true

Opportunity.destroy_all

opportunity = Opportunity.create

flight = opportunity.create_flight ({
  city_from: City.first,
  city_to: City.last,
  date_from: Date.new(2001, 1, 3),
  date_to: Date.new(2001, 2, 3),
  cost: 10_000
})

hotel = opportunity.create_hotel_reservation ({
  city: City.last,
  hotel_name: 'Inn 5 stars',
  date_from: Date.new(2001, 1, 3),
  date_to: Date.new(2001, 2, 3),
  cost: 333_333,
  link: 'http://yandex.ru'
})

User.with_role(:admin).destroy_all
u = User.with_role(:admin).create email: 'cat@mos-it.com', password: '725262'
u.add_role(:admin)
