# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_01_090212) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_stat_statements"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cities", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.float "lat"
    t.float "lng"
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code", comment: "IATA код города."
    t.string "country_code", comment: "Связь для городов"
    t.jsonb "additional_data"
    t.integer "booking_api"
    t.integer "currency"
    t.index ["country_id"], name: "index_cities_on_country_id"
    t.index ["slug"], name: "index_cities_on_slug"
    t.index ["title"], name: "index_cities_on_title"
  end

  create_table "countries", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.float "lat"
    t.float "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code", comment: "IATA код страны."
    t.jsonb "additional_data"
    t.index ["slug"], name: "index_countries_on_slug"
  end

  create_table "data_migrations", primary_key: "version", id: :string, force: :cascade do |t|
  end

  create_table "destinations", force: :cascade do |t|
    t.integer "booking_id"
    t.string "type"
    t.bigint "city_to_id"
    t.bigint "city_from_id"
    t.index ["city_from_id"], name: "index_destinations_on_city_from_id"
    t.index ["city_to_id"], name: "index_destinations_on_city_to_id"
  end

  create_table "flights", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_from_id"
    t.bigint "city_to_id"
    t.bigint "opportunity_id"
    t.date "date_from"
    t.date "date_to"
    t.float "cost"
    t.string "link"
    t.string "airline"
    t.integer "flight_number"
    t.integer "number_of_changes"
    t.integer "source", comment: "Источник информации"
    t.integer "state", default: 0
    t.integer "currency"
    t.datetime "expires_at"
    t.boolean "smallest_price", default: false
    t.string "gate"
    t.index ["airline", "flight_number"], name: "index_flights_on_airline_and_flight_number", unique: true
    t.index ["city_from_id"], name: "index_flights_on_city_from_id"
    t.index ["city_to_id", "date_from", "cost", "date_to"], name: "index_flights_on_city_to_id_and_date_from_and_cost_and_date_to", unique: true
    t.index ["opportunity_id"], name: "index_flights_on_opportunity_id"
  end

  create_table "hotel_reservations", force: :cascade do |t|
    t.bigint "city_id"
    t.float "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "opportunity_id"
    t.date "date_from"
    t.date "date_to"
    t.string "link"
    t.string "hotel_name"
    t.index ["city_id"], name: "index_hotel_reservations_on_city_id"
    t.index ["opportunity_id"], name: "index_hotel_reservations_on_opportunity_id"
  end

  create_table "opportunities", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description", comment: "Текст возможности, описание"
  end

  create_table "pghero_query_stats", force: :cascade do |t|
    t.text "database"
    t.text "user"
    t.text "query"
    t.bigint "query_hash"
    t.float "total_time"
    t.bigint "calls"
    t.datetime "captured_at"
    t.index ["database", "captured_at"], name: "index_pghero_query_stats_on_database_and_captured_at"
  end

  create_table "pghero_space_stats", force: :cascade do |t|
    t.text "database"
    t.text "schema"
    t.text "relation"
    t.bigint "size"
    t.datetime "captured_at"
    t.index ["database", "captured_at"], name: "index_pghero_space_stats_on_database_and_captured_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "saved_filters", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confirmed"
    t.string "email"
    t.string "confirmation_token"
    t.datetime "date_to"
    t.datetime "date_from"
    t.integer "duration", default: 0
    t.integer "max_price", default: 0
    t.bigint "city_from_id"
    t.bigint "city_to_id"
    t.integer "currency"
    t.string "type"
    t.index ["city_from_id"], name: "index_saved_filters_on_city_from_id"
    t.index ["city_to_id"], name: "index_saved_filters_on_city_to_id"
  end

  create_table "subscription_flights", force: :cascade do |t|
    t.bigint "flight_id"
    t.bigint "saved_filter_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flight_id"], name: "index_subscription_flights_on_flight_id"
    t.index ["saved_filter_id"], name: "index_subscription_flights_on_saved_filter_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
  end

  create_table "weather_forecasts", force: :cascade do |t|
    t.bigint "city_id"
    t.datetime "day"
    t.integer "max_temperature"
    t.integer "min_temperature"
    t.jsonb "additional_data"
    t.index ["city_id", "day"], name: "index_weather_forecasts_on_city_id_and_day", unique: true
  end

  add_foreign_key "cities", "countries"
  add_foreign_key "destinations", "cities", column: "city_from_id"
  add_foreign_key "destinations", "cities", column: "city_to_id"
  add_foreign_key "flights", "cities", column: "city_from_id"
  add_foreign_key "flights", "cities", column: "city_to_id"
  add_foreign_key "flights", "opportunities"
  add_foreign_key "hotel_reservations", "cities"
  add_foreign_key "hotel_reservations", "opportunities"
  add_foreign_key "saved_filters", "cities", column: "city_from_id"
  add_foreign_key "saved_filters", "cities", column: "city_to_id"
  add_foreign_key "weather_forecasts", "cities"
end
