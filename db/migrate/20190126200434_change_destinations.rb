class ChangeDestinations < ActiveRecord::Migration[5.2]
  def change
    remove_column :destinations, :city_id
    add_reference :destinations,
                  :city_to,
                  index: true, foreign_key: { to_table: :cities}, default: :null
    add_reference :destinations,
                  :city_from,
                  index: true, foreign_key: { to_table: :cities}, default: :null
  end
end
