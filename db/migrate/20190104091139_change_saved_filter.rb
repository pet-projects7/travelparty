# frozen_string_literal: true

class ChangeSavedFilter < ActiveRecord::Migration[5.2]
  def change
    remove_column :saved_filters, :user_id, :string
    add_column :saved_filters, :confirmed, :boolean
    add_column :saved_filters, :email, :string
    add_column :saved_filters, :confirmation_token, :string
  end
end
