# frozen_string_literal: true

class AddCurrencySavedFilter < ActiveRecord::Migration[5.2]
  def change
    add_column :saved_filters, :currency, :integer
  end
end
