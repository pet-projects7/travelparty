# frozen_string_literal: true

class FillCitiesAndCountriesFromTravelPayouts < ActiveRecord::Migration[5.2]
  def change
    fill_countries
    fill_cities
  end

  private

  def fill_countries
    countries = JSON.parse(File.read('db/migrate/countries.json'))
    countries.each do |country|
      Country.create title: country['name']
    end
  end

  def fill_cities
    cities = JSON.parse(File.read('db/migrate/cities.json'))
    cities.each do |city|
      City.create title: city['name'], code: city['code'], country_code: city['country_code']
    end
  end
end
