# frozen_string_literal: true

class AddDescriptionToOpportunities < ActiveRecord::Migration[5.2]
  def change
    add_column :opportunities, :description, :text, comment: 'Текст возможности, описание'
    add_column :flights, :source, :integer, comment: 'Источник информации'
  end
end
