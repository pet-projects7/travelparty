# frozen_string_literal: true

class AddLinkToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :link, :string
  end
end
