# frozen_string_literal: true

class CreateOpportunities < ActiveRecord::Migration[5.2]
  def change
    create_table :opportunities do |t|
      t.belongs_to :flight, foreign_key: true
      t.belongs_to :hotel_reservation, foreign_key: true

      t.timestamps
    end
  end
end
