class AddStateToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :state, :integer, default: 0
  end
end
