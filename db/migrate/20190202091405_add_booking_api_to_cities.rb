# frozen_string_literal: true

class AddBookingApiToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :booking_api, :integer
  end
end
