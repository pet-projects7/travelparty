# frozen_string_literal: true

class ChangeAttributesInHotel < ActiveRecord::Migration[5.2]
  def change
    remove_column :hotel_reservations, :title
    remove_column :hotel_reservations, :slug
    add_column :hotel_reservations, :link, :string
    add_column :hotel_reservations, :hotel_name, :string
    change_column :hotel_reservations, :cost, :float

    remove_column :flights, :title
    remove_column :flights, :slug
    add_column :flights, :cost, :float
  end
end
