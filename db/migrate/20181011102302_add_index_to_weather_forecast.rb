# frozen_string_literal: true

class AddIndexToWeatherForecast < ActiveRecord::Migration[5.2]
  def change
    WeatherForecast.destroy_all

    add_index(:weather_forecasts, %i[city_id day], unique: true)
  end
end
