# frozen_string_literal: true

class CreateHotelReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :hotel_reservations do |t|
      t.string :title
      t.string :slug
      t.date :date_from
      t.date :date_to
      t.belongs_to :city, foreign_key: true
      t.integer :cost

      t.timestamps
    end
    add_index :hotel_reservations, :slug
  end
end
