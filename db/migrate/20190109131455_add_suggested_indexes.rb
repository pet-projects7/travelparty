# frozen_string_literal: true

class AddSuggestedIndexes < ActiveRecord::Migration[5.2]
  def change
    commit_db_transaction
    add_index :cities, [:title], algorithm: :concurrently
  end
end
