class ChangeSavedFilters < ActiveRecord::Migration[5.2]
  def change
    SavedFilter.destroy_all

    add_reference :saved_filters,
                  :city_from,
                  index: true, foreign_key: { to_table: :cities}, default: :null
    add_reference :saved_filters,
                  :city_to,
                  index: true, foreign_key: { to_table: :cities}, default: :null

    change_column_null(:saved_filters, :city_from, true)
    change_column_null(:saved_filters, :city_to, true)
    change_column_null(:saved_filters, :date_to, true)
    change_column_null(:saved_filters, :date_from, true)
    change_column_null(:saved_filters, :duration, true)
    change_column_null(:saved_filters, :max_price, true)

    remove_column :saved_filters,:city_from
    remove_column :saved_filters,:city_to
  end
end
