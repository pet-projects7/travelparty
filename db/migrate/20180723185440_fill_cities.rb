# frozen_string_literal: true

class FillCities < ActiveRecord::Migration[5.2]
  def change
    require_relative 'cities.rb'
  end
end
