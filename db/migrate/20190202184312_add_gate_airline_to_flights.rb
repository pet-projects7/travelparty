# frozen_string_literal: true

class AddGateAirlineToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :gate, :string
    change_column_default :flights, :number_of_changes, nil
  end
end
