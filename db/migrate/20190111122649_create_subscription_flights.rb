class CreateSubscriptionFlights < ActiveRecord::Migration[5.2]
  def change
    create_table :subscription_flights do |t|
      t.references :flight
      t.references :saved_filter

      t.timestamps
    end
  end
end
