class MoveParamsToDedicatedTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :saved_filters, :params, :jsonb
    add_column :saved_filters, :city_to, :string
    add_column :saved_filters, :city_from, :string
    add_column :saved_filters, :date_to, :datetime
    add_column :saved_filters, :date_from, :datetime
    add_column :saved_filters, :duration, :int
    add_column :saved_filters, :max_price, :int
  end
end
