class ChangeAttributes < ActiveRecord::Migration[5.2]
  def change
    remove_column :flights, :date_from
    remove_column :flights, :date_to

    remove_column :hotel_reservations, :date_from
    remove_column :hotel_reservations, :date_to

    remove_column :opportunities, :flight_id
    remove_column :opportunities, :hotel_reservation_id
  end
end
