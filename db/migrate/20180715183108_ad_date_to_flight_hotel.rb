class AdDateToFlightHotel < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :date_from, :date
    add_column :flights, :date_to, :date

    add_column :hotel_reservations, :date_from, :date
    add_column :hotel_reservations, :date_to, :date
  end
end
