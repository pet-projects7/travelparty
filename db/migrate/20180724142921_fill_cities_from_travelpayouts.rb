class FillCitiesFromTravelpayouts < ActiveRecord::Migration[5.2]
  def up
    destroy_all
    add_columns
  end

  def down
    remove_column :countries, :code
    remove_column :cities, :code
    remove_column :cities, :country_code
  end

  private

  def destroy_all
    HotelReservation.destroy_all
    Flight.destroy_all
    City.destroy_all
    Country.destroy_all
  end

  def add_columns
    add_column :countries, :code, :string, index: true, comment: 'IATA код страны.'
    add_column :cities, :code, :string, index: true, comment: 'IATA код города.'
    add_column :cities, :country_code, :string, index: true, comment: 'Связь для городов'
  end
end
