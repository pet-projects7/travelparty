# frozen_string_literal: true

class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :title
      t.string :slug
      t.float :lat
      t.float :lng

      t.timestamps
    end
    add_index :countries, :slug
  end
end
