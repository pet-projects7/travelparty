class RemoveUnneededIndexes < ActiveRecord::Migration[5.2]
  def change
    remove_index :flights, name: "index_flights_on_city_to_id"
    remove_index :users_roles, name: "index_users_roles_on_user_id"
    remove_index :weather_forecasts, name: "index_weather_forecasts_on_city_id"
  end
end
