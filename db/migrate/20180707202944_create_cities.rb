class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :title
      t.string :slug
      t.float :lat
      t.float :lng
      t.belongs_to :country, foreign_key: true

      t.timestamps
    end
    add_index :cities, :slug
  end
end
