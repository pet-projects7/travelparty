class AddSmallestPriceToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights,
               :smallest_price,
               :boolean, default: false
  end
end
