# frozen_string_literal: true

class AddCurrencyToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :currency, :integer
  end
end
