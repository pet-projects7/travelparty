class AddRealtionshipsToOpportunities < ActiveRecord::Migration[5.2]
  def change
    add_reference :flights, :opportunities, foreign_key: true
    # add_reference :flights, :opportunities, index: true
    # add_foreign_key :flights, :opportunities

    add_reference :hotel_reservations, :opportunities, foreign_key: true
    # add_reference :hotel_reservations, :opportunities, index: true
    # add_foreign_key :hotel_reservations, :opportunities

    remove_column :flights, :country_id
  end
end
