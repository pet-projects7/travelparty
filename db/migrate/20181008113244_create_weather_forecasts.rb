class CreateWeatherForecasts < ActiveRecord::Migration[5.2]
  def change
    create_table :weather_forecasts do |t|
      t.belongs_to :city, foreign_key: true
      t.datetime :day
      t.integer :average_temperature
      t.integer :temperature
    end
  end
end
