# frozen_string_literal: true

class AddFlightNumberAndAirlineToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :airline, :string
    add_column :flights, :flight_number, :int
  end
end
