class ReconfigureWeather < ActiveRecord::Migration[5.2]
  def change
    remove_column :weather_forecasts, :average_temperature
    remove_column :weather_forecasts, :temperature

    add_column :weather_forecasts, :max_temperature, :integer
    add_column :weather_forecasts, :min_temperature, :integer
    add_column :weather_forecasts, :additional_data, :jsonb
  end
end
