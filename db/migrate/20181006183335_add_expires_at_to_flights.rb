# frozen_string_literal: true

class AddExpiresAtToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :expires_at, :datetime unless column_exists?(:flights, :expires_at)
  end
end
