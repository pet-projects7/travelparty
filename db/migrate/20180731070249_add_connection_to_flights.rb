# frozen_string_literal: true

class AddConnectionToFlights < ActiveRecord::Migration[5.2]
  def change
    add_column :flights, :number_of_changes, :integer
    add_index :flights, %i[city_to_id date_from cost date_to], unique: true
  end
end
