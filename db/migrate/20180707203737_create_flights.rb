# frozen_string_literal: true

class CreateFlights < ActiveRecord::Migration[5.2]
  def change
    create_table :flights do |t|
      t.string :title
      t.string :slug
      t.date :date_from
      t.date :date_to
      t.belongs_to :country, foreign_key: true

      t.timestamps

      t.references :city_from, index: true, foreign_key: { to_table: :cities }
      t.references :city_to, index: true, foreign_key: { to_table: :cities }
    end

    add_index :flights, :slug
  end
end
