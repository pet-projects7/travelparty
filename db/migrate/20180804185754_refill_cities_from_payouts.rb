# frozen_string_literal: true

class RefillCitiesFromPayouts < ActiveRecord::Migration[5.2]
  def change
    add_column :countries, :additional_data, :jsonb, if: :not_exist
    add_column :cities, :additional_data, :jsonb, if: :not_exist
  end
end
