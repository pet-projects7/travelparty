class AddCurrencyToCities < ActiveRecord::Migration[5.2]
  def change
    unless column_exists?(:cities, :currency)
      add_column :cities, :currency, :integer
    end
  end
end
