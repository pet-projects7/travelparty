# frozen_string_literal: true

class CreateDestinations < ActiveRecord::Migration[5.2]
  def change
    create_table :destinations do |t|
      t.belongs_to :city, foreign_key: true
      t.integer :booking_id
      t.integer :currency
      t.string :type
    end
  end
end
