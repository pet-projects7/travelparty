class AddUniqIndexOnSavedFilters < ActiveRecord::Migration[5.2]
  def change
    add_index :saved_filters, [:email, :params], unique: true
  end
end
