class CreateSavedFilters < ActiveRecord::Migration[5.2]
  def change
    create_table :saved_filters do |t|
      t.belongs_to :user, foreign_key: true
      t.jsonb :params

      t.timestamps
    end
  end
end
