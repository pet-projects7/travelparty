class RenameColumnsOpportunity < ActiveRecord::Migration[5.2]
  def change
    rename_column :flights, :opportunities_id, :opportunity_id
    rename_column :hotel_reservations, :opportunities_id, :opportunity_id
  end
end
