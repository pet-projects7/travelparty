class RemoveCurrencyFromDestinations < ActiveRecord::Migration[5.2]
  def change
    remove_column :destinations, :currency
  end
end
