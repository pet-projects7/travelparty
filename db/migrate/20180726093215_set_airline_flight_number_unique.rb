class SetAirlineFlightNumberUnique < ActiveRecord::Migration[5.2]
  def change
    add_index :flights, [:airline, :flight_number], unique: true
  end
end
