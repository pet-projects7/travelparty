# frozen_string_literal: true

class SavedFiltersController < ApplicationController
  before_action :set_saved_filter, only: %i[show edit update destroy]

  def index
    @saved_filters = SavedFilter.already_confirmed
  end

  def show
    @flights = @saved_filter.flights.paginate(page: params[:page], per_page: 100).order(created_at: :desc)
  end

  def new
    return @saved_filter = SavedFilter.new unless filter_from_list_params.present?

    @saved_filter = SavedFilter.new
    @saved_filter.city_to = City.find_by(code: filter_from_list_params[:city]) if filter_from_list_params[:city].present?
    @saved_filter.city_from = City.find(filter_from_list_params[:city_from_id]) if filter_from_list_params[:city_from_id].present?
    @saved_filter.date_to = filter_from_list_params[:date_to] if filter_from_list_params[:date_to].present?
    @saved_filter.date_from = filter_from_list_params[:date_from] if filter_from_list_params[:date_from].present?
    @saved_filter.duration = filter_from_list_params[:duration] if filter_from_list_params[:duration].present?
    @saved_filter.max_price = filter_from_list_params[:max_price] if filter_from_list_params[:max_price].present?
  end

  def edit; end

  def create
    @saved_filter = SavedFilter.new(saved_filter_params)
    @saved_filter.confirmation_token = SecureRandom.uuid

    respond_to do |format|
      if @saved_filter.save
        format.html { redirect_to new_saved_filter_path, notice: 'Saved filter was successfully created.' }
        format.json do
          render json: { success: true }
        end
      else
        format.html { render :new }
        format.json { render json: @saved_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @saved_filter.update(saved_filter_params)
        format.html { redirect_to @saved_filter, notice: 'Saved filter was successfully updated.' }
        format.json { render :show, status: :ok, location: @saved_filter }
      else
        format.html { render :edit }
        format.json { render json: @saved_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @saved_filter.destroy
    respond_to do |format|
      format.html { redirect_to saved_filters_url, notice: 'Saved filter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def activate
    @token = SavedFilter.find_by(confirmation_token: params[:token])
    @token.update(confirmed: true)
  end

  def deactivate
    saved_filter = GlobalID::Locator.locate_signed(params[:token], for: :unsubscribe)

    respond_to do |format|
      if saved_filter.present?
        saved_filter&.delete
        format.html { render :deactivate, notice: 'Вы успешно отписаны от рассылки' }
      else
        format.html { render :deactivate, notice: 'Возможно срок действия токена истёк' }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_saved_filter
    @saved_filter = SavedFilter.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def saved_filter_params
    params.require(:saved_filter).permit(:email, :city_to_id, :city_from_id, :date_to, :date_from, :duration, :max_price)
  end

  def filter_from_list_params
    params.require(:saved_filter).permit(:city, :city_from_id, :date_from, :date_to, :duration, :max_price) if params[:saved_filter]
  end
end
