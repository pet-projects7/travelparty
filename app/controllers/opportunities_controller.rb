# frozen_string_literal: true

class OpportunitiesController < ApplicationController
  before_action :set_opportunity, only: %i[show edit update destroy]

  # GET /opportunities
  # GET /opportunities.json
  def index
    @opportunities = Opportunity.includes(:flight, :hotel_reservation).all
  end

  # GET /opportunities/1
  # GET /opportunities/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_opportunity
    @opportunity = Opportunity.find(params[:id])
  end
end
