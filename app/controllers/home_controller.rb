# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @cities = Rails.cache.fetch('home_cities') do
      City.enabled_departure_cities
    end
  end
end
