class WeatherForecastsController < ApplicationController
  def get_weather_forecast
    date_range = Date.parse(get_params[:dateFrom])..Date.parse(get_params[:dateTo])
    forecast = WeatherForecast
                 .where(day: date_range, city_id: get_params[:city])
                 .select(:max_temperature, :min_temperature, :additional_data, :day)

    render json: forecast
  end

  private

  def get_params
    params.require(:filter).permit(:dateFrom, :dateTo, :city)
  end
end
