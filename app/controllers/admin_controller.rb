# frozen_string_literal: true

class AdminController < ActionController::Base
  layout 'admin'
  before_action :authenticate_user!, :authorize_user!

  def index
    @cities = City.enabled_departure_cities
  end

  private

  def authorize_user!
    redirect to: root_path unless current_user.has_role?(:admin)
  end
end
