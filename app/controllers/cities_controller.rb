# frozen_string_literal: true

class CitiesController < ApplicationController
  before_action :set_city, only: %i[show search flights_statistics flights anywhere]
  before_action :set_cities_by_code, only: %i[flights_to_selected_city]

  def show
    # @opportunities = CityOpportunitiesQuery.new.call(@city.opportunities, params)
    @fligts_count = Flight.where(city_from_id: @city.id).group(:city_to_id).count

    @destinations = Destination
                      .where(city_from_id: @city.id, city_to_id: @fligts_count.keys)
                      .includes(:city_to, :city_from)
                      .order('cities.title')
                      .group_by { |destination| destination.city_to.country_code }

    @countries = Country
                   .where(code: @destinations.keys)
                    .with_attached_flag
                   .order(title: :asc)
    # @minimal_prices = Flight
    #                     .order(:cost)
    #                     .distinct(:city_to_id)
    #                     .where(city_to_id: @fligts_count.keys)

  end

  def flights
    handle_flights_from_cache
  end

  def anywhere
    handle_flights_from_cache
    @url = flights_to_anywhere_path(@city)
    render :flights
  end

  def flights_to_selected_city
    @url = flights_to_selected_city_path(@city.code, @city_to.code)
    handle_flights_from_cache
  end

  def search
    @cities = City
                .where(id: Flight.find_by_sql("SELECT DISTINCT city_to_id FROM flights WHERE city_from_id = #{@city.id}").pluck(:city_to_id))
  end

  private

  def set_city
    @city = City.find(params[:id])
  end

  def set_cities_by_code
    @city = City.find_by_code(params[:city_from].upcase)
    @city_to = City.find_by_code(params[:city_to].upcase)
    @destination = Destination.where(city_from_id: @city.id, city_to_id: @city_to.id).limit(1).first
    @flight_with_min_price = @destination&.flight_with_min_price
  end

  def handle_flights_from_cache
    @flights = Rails.cache.fetch("#{request.url}/flights", expires_in: 10.minutes)
    @flights_cities = Rails.cache.fetch("#{request.url}/flights_cities", expires_in: 10.minutes)
    @cities = Rails.cache.fetch("#{request.url}/cities", expires_in: 10.minutes)

    fetch_flights_from_cache unless @flights && @flights_cities && @cities
  end

  def fetch_flights_from_cache
    @flights = @city
               .departing_flights
               .order_scope(params[:order] || nil, params[:direction])
               .order(created_at: :desc)
               .includes(:city_from)
               .includes(:city_to)
               .paginate(page: params[:page], per_page: 100)
               .by_including_dates(params[:date_in] || nil)
               .by_arriving_city(params[:city] || @city_to&.code || nil)
               .by_source(params[:source] || nil)
               .by_departing_date(params[:date_from] || nil)
               .by_arriving_date(params[:date_to] || nil)
               .by_max_price(params[:max_price] || nil)

    Rails.cache.write("#{request.url}/flights", @flights)

    @flights_cities = @city
                      .departing_flights
                      .includes(:city_from)
                      .includes(:city_to)
                      .by_departing_date(params[:date_from] || nil)
                      .by_arriving_date(params[:date_to] || nil)
                      .by_max_price(params[:max_price] || nil)
                      .distinct('flights.city_to_id')
                      .pluck('flights.city_to_id')

    Rails.cache.write("#{request.url}/flights_cities", @flights_cities)

    @cities = City.where(id: @flights_cities).order(title: :asc)

    Rails.cache.write("#{request.url}/cities", @cities)
  end
end
