# frozen_string_literal: true

module Admin
  class OpportunitiesController < AdminController
    before_action :set_opportunity, only: %i[show edit update destroy]
    before_action :set_city, only: %i[index show edit update destroy]

    # GET /opportunities
    # GET /opportunities.json
    def index
      @opportunities = Opportunity.all
    end

    # GET /opportunities/1
    # GET /opportunities/1.json
    def show; end

    # GET /opportunities/new
    def new
      @opportunity = Opportunity.new
      @opportunity.build_hotel_reservation
      @opportunity.build_flight
    end

    # GET /opportunities/1/edit
    def edit; end

    # POST /opportunities
    # POST /opportunities.json
    def create
      @opportunity = Opportunity.new(description: opportunity_params[:description])
      @opportunity.flight = Flight.find(opportunity_params['flight_attributes']['id'])
      @opportunity.image = nil

      respond_to do |format|
        if @opportunity.save
          format.html { redirect_to admin_city_opportunities_path, notice: 'Opportunity was successfully created.' }
        # format.json { render :show, status: :created, location: @opportunity }
        else
          format.html { render :new }
          # format.json { render json: @opportunity.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /opportunities/1
    # PATCH/PUT /opportunities/1.json
    def update
      respond_to do |format|
        if @opportunity.update(opportunity_params)
          format.html { redirect_to @opportunity, notice: 'Opportunity was successfully updated.' }
          format.json { render :show, status: :ok, location: @opportunity }
        else
          format.html { render :edit }
          format.json { render json: @opportunity.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /opportunities/1
    # DELETE /opportunities/1.json
    def destroy
      @opportunity.destroy
      respond_to do |format|
        format.html { redirect_to opportunities_url, notice: 'Opportunity was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_opportunity
      @opportunity = Opportunity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def opportunity_params
      params
        .require(:opportunity)
        .permit(:flight_id,
                :hotel_reservation_id,
                :image,
                :description,
                hotel_reservation_attributes: %i[id title],
                flight_attributes: %i[id title])
    end

    def set_city
      @city = City.find(params[:city_id])
    end
  end
end
