# frozen_string_literal: true

class Admin::CitiesController < AdminController
  before_action :set_city, only: %i[show edit update destroy month_flights cheapest_flights popular_directions latest destinations stats]
  skip_before_action :authenticate_user!, :authorize_user!, only: [:show]

  def index
    @cities = City.enabled_departure_cities
  end

  def flights_statistics
    render json: Flight.statistics(@city.id).to_json
  end

  def show
    @flights = @city
               .departing_flights
               .order_scope(params[:order] || nil, params[:direction])
               .order(created_at: :desc)
               .includes(:city_from)
               .includes(:city_to)
               .paginate(page: params[:page], per_page: 100)
               .by_arriving_city(params[:city] || nil)
               .by_source(params[:source] || nil)
               .by_departing_date(params[:departure_date_from] || nil)
               .by_arriving_date(params[:arrive_date_from] || nil)

    @cities = City
              .where(id: Flight.find_by_sql("SELECT DISTINCT city_to_id FROM flights WHERE city_from_id = #{@city.id}")
                .pluck(:city_to_id))
  end

  def new
    @city = City.new
  end

  def edit; end

  def create
    @city = City.new(city_params)

    respond_to do |format|
      if @city.save
        format.html { redirect_to @city, notice: 'City was successfully created.' }
        format.json { render :show, status: :created, location: @city }
      else
        format.html { render :new }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to @city, notice: 'City was successfully updated.' }
        format.json { render :show, status: :ok, location: @city }
      else
        format.html { render :edit }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to cities_url, notice: 'City was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def month_flights
    @flights = FlightSearchService.month
    # @flights.sort do |flight1, flight2|
    # Date.parse(flight1['depart_date']) < Date.parse(flight2['depart_date'])
    # p flight1.inspect
    # p Date.parse(flight1['depart_date'])
    # p '------------'
    # p flight2.inspect
    # p Date.parse(flight2['depart_date'])
    # end
    @cities = City.where(code: @flights.pluck('destination'))
  end

  def cheapest_flights
    @flights = FlightSearchService.cheapest
    @cities = City.where(code: @flights.keys)
  end

  def popular_directions
    @directions = FlightSearchService.popular_directions
    @cities = City.where(code: @directions.keys)
  end

  def destinations
    @destinations = @city.destinations.includes(:city_to)
  end

  def stats
    @latest = @city
              .departing_flights
              .where(source: :latest)
              .group_by_day(:created_at, range: 3.month.ago.to_date..Date.today, format: '%Y-%m-%d')
              .count

    @direct = @city
              .departing_flights
              .where(source: :direct)
              .group_by_day(:created_at, range: 3.month.ago.to_date..Date.today, format: '%Y-%m-%d')
              .count

    @nearest = @city
               .departing_flights
               .where(source: :nearest)
               .group_by_day(:created_at, range: 3.month.ago.to_date..Date.today, format: '%Y-%m-%d')
               .count

    @cheapest = @city
                .departing_flights
                .where(source: :nearest)
                .group_by_day(:created_at, range: 3.month.ago.to_date..Date.today, format: '%Y-%m-%d')
                .count

    @sourceless = @city
                  .departing_flights
                  .where(source: nil)
                  .group_by_day(:created_at, range: 3.month.ago.to_date..Date.today, format: '%Y-%m-%d')
                  .count
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_city
    @city = City.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def city_params
    params.require(:city).permit(:title, :slug, :lat, :lng, :country_id, :departure_date_from)
  end
end
