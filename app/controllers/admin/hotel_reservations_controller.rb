# frozen_string_literal: true

class Admin::HotelReservationsController < ApplicationController
  before_action :set_hotel_reservation, only: %i[show edit update destroy]

  # GET /hotel_reservations
  # GET /hotel_reservations.json
  def index
    @hotel_reservations = HotelReservation.all
  end

  # GET /hotel_reservations/1
  # GET /hotel_reservations/1.json
  def show; end

  # GET /hotel_reservations/new
  def new
    @hotel_reservation = HotelReservation.new
  end

  # GET /hotel_reservations/1/edit
  def edit; end

  # POST /hotel_reservations
  # POST /hotel_reservations.json
  def create
    @hotel_reservation = HotelReservation.new(hotel_reservation_params)

    respond_to do |format|
      if @hotel_reservation.save
        format.html { redirect_to @hotel_reservation, notice: 'Hotel reservation was successfully created.' }
        format.json { render :show, status: :created, location: @hotel_reservation }
      else
        format.html { render :new }
        format.json { render json: @hotel_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hotel_reservations/1
  # PATCH/PUT /hotel_reservations/1.json
  def update
    respond_to do |format|
      if @hotel_reservation.update(hotel_reservation_params)
        format.html { redirect_to @hotel_reservation, notice: 'Hotel reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @hotel_reservation }
      else
        format.html { render :edit }
        format.json { render json: @hotel_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hotel_reservations/1
  # DELETE /hotel_reservations/1.json
  def destroy
    @hotel_reservation.destroy
    respond_to do |format|
      format.html { redirect_to hotel_reservations_url, notice: 'Hotel reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_hotel_reservation
    @hotel_reservation = HotelReservation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hotel_reservation_params
    params.require(:hotel_reservation).permit(:title, :slug, :date_from, :date_to, :city_id, :cost)
  end
end
