# frozen_string_literal: true

module WeatherForecastsHelper
  def average_temperature(forecast)
    (forecast.max_temperature + forecast.min_temperature) / 2
  end
end
