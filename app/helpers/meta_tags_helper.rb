# frozen_string_literal: true

module MetaTagsHelper
  def general_meta_tags
    page = "#{controller_path}_#{action_name}_tags".tr('/', '_').to_sym

    page_tags = send(page) if respond_to?(page)

    default_meta.update(page_tags || {})
  end

  def default_meta
    {
      reverse: true,
      site: 'Vityan.com',
      title: t('project_name'),
      description: t('project_description'),
      keywords: t('keywords'),
      og: {
        title: :title,
        site_name: :site
      }
    }
  end

  def cities_anywhere_tags
    {
      title: 'Куда нибудь задёшево',
      description: 'Самые выгодные билеты в Европейские столицы',
      keywords: t('keywords'),
      og: {
        title: :title,
        site_name: :site
      }
    }
  end

  def cities_flights_to_selected_city_tags
    minimal_price = "от #{@flight_with_min_price.cost} #{@city.currency}" if @flight_with_min_price.present?

    {
      title: "Авиабилеты #{@city.title} - #{@city_to.title} #{minimal_price}",
      description: "Авиабилеты #{@city} - #{@city_to}",
      keywords: t('keywords'),
      og: {
        title: :title,
        site_name: :site
      }
    }
  end
end
