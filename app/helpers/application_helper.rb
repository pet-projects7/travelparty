# frozen_string_literal: true

module ApplicationHelper
  def svg_icon(icon_name, attrs = {})
    icon_url = "#{asset_url('svg_icons.svg')}##{icon_name}"
    use_tag = content_tag(:use, nil, 'xlink:href': icon_url)
    content_tag(:svg, use_tag, attrs)
  end

  def enabled_departure_cities
    City.enabled_departure_cities.all
  end

  def sort_flights_link(column_name)
    url_for(order: column_name,
            direction: params[:direction] == 'asc' ? :desc : :asc,
            city: params[:city] || nil,
            date_from: params[:date_from] || nil,
            date_to: params[:date_to] || nil,
            max_price: params[:max_price] || nil,
            duration: params[:duration] || nil,
            source: params[:source] || nil)
  end

  def booking_link(_booking_city_id, _date_from, _date_to)
    "https://www.booking.com/searchresults.ru.html?city=-850553
      &nflt=review_score%253D90%253Breview_score%253D80
      &aid=1627894
      &checkin_monthday=17
      &checkin_month=10
      &checkin_year=2018
      &checkout_monthday=25
      &checkout_month=10
      &checkout_year=2018
      &no_rooms=1&group_adults=1"
  end
end
