# frozen_string_literal: true

module CitiesHelper
  def city_name_by_locale(city)
    if locale == :ru
      city.title
    else
      city&.additional_data.dig('name_translations', 'en') || city.title
    end
  end

  def settings
    {
      MOW: {
        currency: :rub,
        locale: :ru
      },
      NYC: {
        currency: :usd,
        locale: :en
      }
    }
  end

  def flight_cities_destinations(flight_cities)
    featured = flight_cities.select { |city| city.featured == true }
    not_featured = flight_cities - featured

    [featured, not_featured]
  end
end
