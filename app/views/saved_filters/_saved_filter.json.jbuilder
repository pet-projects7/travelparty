# frozen_string_literal: true

json.extract! saved_filter, :id, :user_id, :params, :created_at, :updated_at
json.url saved_filter_url(saved_filter, format: :json)
