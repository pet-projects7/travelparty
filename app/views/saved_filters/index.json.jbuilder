# frozen_string_literal: true

json.array! @saved_filters, partial: 'saved_filters/saved_filter', as: :saved_filter
