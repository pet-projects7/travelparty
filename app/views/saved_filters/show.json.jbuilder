# frozen_string_literal: true

json.partial! 'saved_filters/saved_filter', saved_filter: @saved_filter
