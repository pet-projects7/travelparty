# frozen_string_literal: true

json.array! @flights, partial: 'flights/flight', as: :flight
