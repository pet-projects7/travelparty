# frozen_string_literal: true

json.extract! flight, :id, :title, :slug, :date_from, :date_to, :country_id, :created_at, :updated_at
json.url flight_url(flight, format: :json)
