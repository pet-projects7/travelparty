# frozen_string_literal: true

json.partial! 'flights/flight', flight: @flight
