# frozen_string_literal: true

json.extract! opportunity, :id, :flight_id, :hotel_reservation_id, :created_at, :updated_at
json.url opportunity_url(opportunity, format: :json)
