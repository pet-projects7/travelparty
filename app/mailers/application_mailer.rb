# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'Vityan Subscriptions <subscriptions@vityan.com>'
  layout 'mailer'

  private

  def generate_unsubscribe_link
    @unsubscribe_link = params[:filter].to_sgid_param(expires_in: 1.day, for: :unsubscribe)
  end
end
