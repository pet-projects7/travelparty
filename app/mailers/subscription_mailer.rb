# frozen_string_literal: true

class SubscriptionMailer < ApplicationMailer
  layout 'mailer'

  def verify_subscription
    mail(to: params[:email], subject: 'Пожалуйста, подтвердите вашу подписку')
  end

  def new_flights
    generate_unsubscribe_link

    mail(to: params[:email], subject: 'Новые билеты по вашей подписке')
    # ses = AWS::SES::Base.new( ... connection info ... )
    #
    # ses.send_email(
    #   :to        => [params[:email]],
    #   :source    => '"Vityan.com" <info@vityan.com>',
    #   :subject   => 'Новые билеты по вашей подписке',
    #   :text_body => 'Internal text body'
    # )
  end
end
