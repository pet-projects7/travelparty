# frozen_string_literal: true

class City < ApplicationRecord
  enum currency: { USD: 0, RUB: 1, EUR: 2, UAH: 3, BYN: 4 }
  attr_accessor :featured

  belongs_to :country,
             foreign_key: 'country_code',
             primary_key: 'code',
             optional: true

  scope :enabled_departure_cities, -> { where(title: Settings.enabled_departure_cities.keys) }
  scope :enabled_ariving_cities, -> { where(title: Settings.europe_arriving_cities.keys) }

  has_many :departing_flights, class_name: 'Flight', foreign_key: :city_from_id
  has_many :arriving_flights, class_name: 'Flight', foreign_key: :city_to_id

  has_many :destinations, class_name: 'Destination', foreign_key: :city_from_id

  has_many :opportunities, through: :departing_flights, foreign_key: 'id'

  has_many :hotel_reservations

  # has_many :images
  has_many_attached :images, dependent: :destroy

  has_many_attached :covers, dependent: :destroy
  # has_one :destination
  #
  def flight_with_min_price
    Flight
      .where(city_to_id: id, currency: :RUB)
      .order(:cost)
      .limit(1)
      &.first
  end
end
