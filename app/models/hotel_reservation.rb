# frozen_string_literal: true

class HotelReservation < ApplicationRecord
  belongs_to :city, optional: true
  belongs_to :opportunity

  validates :date_from, :date_to, :hotel_name, :link, :cost, presence: true
end
