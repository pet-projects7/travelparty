# frozen_string_literal: true

class SubscriptionFlight < ApplicationRecord
  belongs_to :flight
  belongs_to :saved_filter
end
