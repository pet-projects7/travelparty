# frozen_string_literal: true

class SavedFilter < ApplicationRecord
  delegate :url_helpers, to: 'Rails.application.routes'

  validates :email, presence: true
  has_many :subscription_flights
  has_many :flights, through: :subscription_flights

  after_create :send_confirmation_letter

  belongs_to :city_from, class_name: 'City', optional: true, foreign_key: :city_from_id
  belongs_to :city_to, class_name: 'City', optional: true, foreign_key: :city_to_id

  def send_confirmation_letter
    params = {
      token: confirmation_token,
      email: email,
      description: description
    }

    SubscriptionMailer
      .with(params)
      .verify_subscription
      .deliver_later
  end

  scope :already_confirmed, -> { where(confirmed: true) }

  def description
    description = ''
    filter_values = serializable_hash

    filter_values.each do |key, value|
      next if %w[id created_at updated_at confirmed confirmation_token city_from_id city_to_id date_to date_from].include?(key)
      next if value.nil?

      key_translate = "helpers.label.#{key}"
      description += "#{I18n.t(key_translate)} - #{value} "
    end

    description += "#{I18n.t('helpers.label.city_to')} - #{city_to.title} " if city_to_id
    description += "#{I18n.t('helpers.label.city_from')} - #{city_from.title} " if city_from_id

    description += "#{I18n.t('helpers.label.date_to')} - #{I18n.localize(date_to, format: '%d %b')} " if date_to
    description += "#{I18n.t('helpers.label.date_from')} - #{I18n.localize(date_from, format: '%d %b')} " if date_to
    description
  end

  def relevant_flights
    FlightsRelevantToSavedFilterQuery.new(Flight.all).call(self)
  end

  def link
    default_params = {
      max_price: max_price,
      date_from: date_from&.strftime('%d.%m.%Y'),
      date_to: date_to&.strftime('%d.%m.%Y'),
      duration: duration
    }

    if city_to_id
      url_helpers.flights_to_selected_city_path(city_from.code, city_to.code, default_params)
    else
      url_helpers.flights_to_anywhere_path(city_from || City.find_by_title('Москва'), default_params)
    end
  end
end
