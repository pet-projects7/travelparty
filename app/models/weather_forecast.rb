# frozen_string_literal: true

class WeatherForecast < ApplicationRecord
  belongs_to :city
end
