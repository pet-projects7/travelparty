# frozen_string_literal: true

class Opportunity < ApplicationRecord
  has_one :flight, dependent: :destroy
  has_one :hotel_reservation, dependent: :destroy
  has_one_attached :image, dependent: :destroy

  accepts_nested_attributes_for :flight, :hotel_reservation, allow_destroy: true

  def duration
    (flight.date_to - flight.date_from).to_i
  end
end
