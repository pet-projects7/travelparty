# frozen_string_literal: true

class Flight < ApplicationRecord
  # after_create :get_weather
  after_create :find_relevant_subscriptions

  enum source: { cheapest: 1, latest: 2, nearest: 3, direct: 4 }
  enum state: { active: 0, archived: 0 }
  enum currency: { USD: 0, RUB: 1, EUR: 2, UAH: 3, BYN: 4 }

  belongs_to :opportunity, optional: :true

  # validates :date_from, :date_to, :cost, :city_to_id, presence: true

  belongs_to :city_from, class_name: 'City', optional: true, foreign_key: :city_from_id
  belongs_to :city_to, class_name: 'City', optional: true, foreign_key: :city_to_id

  scope :order_scope, ->(col, direction = :asc) { eval("order(#{col}: :#{direction})") if col.present? }

  scope :by_arriving_city, lambda { |_city|
    return self unless _city.present?

    joins('INNER JOIN cities AS arriving_cities ON arriving_cities.id = flights.city_to_id')
      .where("arriving_cities.code = '#{_city}'")
  }

  scope :by_source, lambda { |source|
    return self unless source.present?

    where(source: source)
  }

  scope :by_max_price, lambda { |price|
    return self unless price.present?

    where('cost <= ?', price)
  }

  scope :by_departing_date, lambda { |dates|
    return self unless dates.present?

    date = Date.strptime(dates, '%d.%m.%Y')
    where('DATE(date_from) >= ?', date)
  }

  scope :by_arriving_date, lambda { |dates|
    return self unless dates.present?

    date = Date.strptime(dates, '%d.%m.%Y')
    where('DATE(date_to) <= ?', date)
  }

  scope :by_including_dates, lambda { |dates|
    return self unless dates.present?

    dates = dates.split(',')
    return self if dates.count != 2

    date_from = Date.strptime(dates[0], '%d.%m.%Y')
    date_to = Date.strptime(dates[1], '%d.%m.%Y')

    where('DATE(date_from) <= ? AND DATE(date_to) >= ?', date_from, date_to)
  }

  scope :expired, lambda {
    Flight
      .where('DATE(date_from) < ?', Date.today)
      .where('opportunity_id IS ?', nil)
      .where(smallest_price: false)
      .or(Flight.where('DATE(expires_at) > ?', Date.today))
      .or(Flight.where("created_at < DATE('#{3.months.ago.strftime('%Y-%m-%d')}')"))
  }

  scope :broken, lambda {
    where('city_to_id is ?', nil)
      .where(smallest_price: false)
      .where(cost: nil)
      .or(Flight.where('city_from_id is ?', nil))

    # .or(Flight.where("created_at < DATE('#{ 3.months.ago.strftime('%Y-%m-%d') }')"))
  }

  scope :statistics, lambda { |city_id|
    where(created_at: 30.days.ago..Date.current)
      .where(city_from_id: city_id)
      .where(smallest_price: false)
      .group('date(flights.created_at)')
      .count
  }

  def duration
    return 0 unless date_to.present? && date_from.present?

    (date_to - date_from).to_i
  end

  def link
    content = 'https://hydra.aviasales.ru/searches/new?marker=182694'
    content += "&origin_iata=#{city_from.code}"
    content += "&destination_iata=#{city_to.code}"
    content += "&depart_date=#{date_from}"
    content += "&return_date=#{date_to}&with_request=true"

    content
  end

  def booking_link
    booking_id = city_to.booking_api

    return nil if booking_id.nil?

    "https://www.booking.com/searchresults.ru.html?city=#{booking_id}
      &nflt=review_score%253D90%253Breview_score%253D80
      &aid=1627894
      &checkin_monthday=#{date_from.day}
      &checkin_month=#{date_from.month}
      &checkin_year=#{date_from.year}
      &checkout_monthday=#{date_to.day}
      &checkout_month=#{date_from.month}
      &checkout_year=#{date_from.year}
      &no_rooms=1
      &group_adults=1"
  end

  def weather_forecasts
    WeatherForecast.where(city_id: city_to_id, day: date_from..date_to)
  end

  def get_weather
    WeatherForFlightWorker.perform_async
  end

  def find_relevant_subscriptions
    flight_params = {
      city_to: city_to_id,
      city_from: city_from_id,
      date_from: date_from,
      date_to: date_to,
      price: cost,
      duration: duration,
      city_to_title: city_to&.title,
      id: id
    }

    RelevantFlightsSearcherWorker.perform_async(flight_params)
    # RelevantFlightsSearcherWorker.new.perform(flight_params)
  end

  def email_row
    # <td class='table__cell'>#{ self.city_to.title }</td>

    "
      <tr class='table__row'>
        <td class='table__cell'>#{I18n.localize(date_from, format: '%d %b')}</td>
        <td class='table__cell'>#{I18n.localize(date_to, format: '%d %b')}</td>
        <td class='table__cell'>#{cost}</td>
        <td class='table__cell'><a href='#{link}'>Aviasales</a></td>
      </tr>
    "
  end
end
