# frozen_string_literal: true

class Destination < ApplicationRecord
  enum currency: { USD: 1, RUB: 2 }
  belongs_to :city_from, class_name: 'City', optional: true, foreign_key: :city_from_id
  belongs_to :city_to, class_name: 'City', optional: true, foreign_key: :city_to_id

  has_one :cheapest_flight, -> { Flight.where(city_to_id: city_to_id, city_from_id: city_from_id) }

  scope :home_cities, lambda {
    City.where(id: select(:city_from_id).distinct.pluck(:city_from_id))
  }

  def flight_with_min_price
    Flight.where(city_to_id: city_to_id, currency: city_from.currency)
          .order(:cost)
          .first
  end

  def self.json_for_parse
    currencies = City.currencies

    destinations = Destination
                   .joins(:city_from, :city_to)
                   .pluck('cities.code',
                          'cities.id',
                          'city_tos_destinations.code',
                          'city_tos_destinations.id',
                          'cities.currency')
    destinations = destinations.map do |cities_array|
      {
        city_from_code: cities_array[0],
        city_from_id: cities_array[1],
        city_to_code: cities_array[2],
        city_to_id: cities_array[3],
        currency: currencies.key(cities_array[4])
      }
    end

    destinations
  end
end
