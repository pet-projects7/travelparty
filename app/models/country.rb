# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :cities, foreign_key: 'country_code', primary_key: 'code'
  has_one_attached :flag
end
