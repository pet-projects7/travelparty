# frozen_string_literal: true

class CrawlWeatherForecastWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'weather', threshold: 40, period: 1.hour

  def perform(cityID, lat, lng, date)
    @cityID = cityID
    @lat = lat
    @lng = lng
    @date = date

    # @city = City.find(message['cityId'])
    # date_from = Date.parse(message['dateFrom'])
    # date_to = Date.parse(message['dateTo'])
    #
    # @lat = @city['additional_data']['coordinates']['lat']
    # @lng = @city['additional_data']['coordinates']['lon']

    request
    end

    private

  def request
    response = WeatherService.new(@lat, @lng, @date.to_time.to_i).request

    data = response['daily']['data'][0]

    WeatherForecast.find_or_create_by(city_id: @cityID, day: @date) do |record|
      record.day = @date
      record.max_temperature = data['apparentTemperatureHigh']
      record.min_temperature = data['apparentTemperatureLow']
      record.additional_data = data
    end

    # forecast = { city_id: @cityID,
    #    day: @date,
    #    max_temperature: data['apparentTemperatureHigh'],
    #    min_temperature: data['apparentTemperatureLow'],
    #    additional_data: data
    # }
    #
    # WeatherForecast.create forecast
  rescue ActiveRecord::RecordNotUnique
  end
  end
