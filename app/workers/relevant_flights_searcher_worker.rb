# frozen_string_literal: true

class RelevantFlightsSearcherWorker
  include Sidekiq::Worker

  def perform(flight)
    filters = SavedFilterQuery.new(SavedFilter.already_confirmed).call(flight)

    filter_ids = filters.collect { |filter| { saved_filter_id: filter.id, flight_id: flight['id'] } }

    SubscriptionFlight.create filter_ids
  end
end
