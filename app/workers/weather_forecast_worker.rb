# frozen_string_literal: true

class WeatherForecastWorker
  include Sidekiq::Worker

  def perform(message)
    @city = City.find(message['cityId'])
    date_from = Date.parse(message['dateFrom'])
    date_to = Date.parse(message['dateTo'])

    @lat = @city['additional_data']['coordinates']['lat']
    @lng = @city['additional_data']['coordinates']['lon']

    @dates = (date_from..date_to).to_a

    request
  end

  private

  def request
    ActionCable.server.broadcast 'chat_1', title: 'New things!', body: "All that's fit for print"

    # forecasts = []
    #
    # @dates.map do |date|
    #   response = WeatherService.new(@lat, @lng, date.to_time.to_i).request
    #
    #   data = response['daily']['data'][0]
    #
    #   forecasts << {
    #     city_id: @city.id,
    #     day: date,
    #     max_temperature: data['apparentTemperatureHigh'],
    #     min_temperature: data['apparentTemperatureLow'],
    #     additional_data: data
    #   }
    # end
    #
    # WeatherForecast.import forecasts

    # WeatherForecastChannel.broadcast 'chat_1', { forecasts: forecasts }
  end
end
