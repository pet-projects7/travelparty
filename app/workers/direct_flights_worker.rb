# frozen_string_literal: true

class DirectFlightsWorker < BaseFlightParserWorker
  include Sidekiq::Worker

  def perform(cities)
    currency = cities['currency'] || cities[:currency]

    flights = get_flights(cities['city_from_code'] || cities[:city_from_code], cities['city_to_code'] || cities[:city_to_code], currency) || []

    unless flights.empty?
      creation_arr = flights.map do |flight|
        {
          source: :direct,
          city_from_id: cities['city_from_id'],
          city_to_id: cities['city_to_id'],
          date_from: flight['departure_at'],
          date_to: flight['return_at'],
          cost: flight['price'],
          currency: currency.to_sym,
          number_of_changes: 0,
          airline: flight['price'],
          flight_number: flight['flight_number']
        }
      end

      creation_arr = flight_fine?(creation_arr)

      created_flights = create_flights(creation_arr)

      unless created_flights.ids.empty?
        created_flights.ids.each { |flight_id| Flight.find(flight_id).find_relevant_subscriptions }
      end
    end
  end

  private

  def get_flights(departure, destination, currency)
    FlightSearchService.direct(departure, destination, currency)
  end

  def create_flights(flights)
    Flight.import flights, on_duplicate_key_ignore: true
  rescue ActiveRecord::RecordNotUnique => error
    true
  end
end
