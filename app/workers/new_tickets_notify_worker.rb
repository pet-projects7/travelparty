# frozen_string_literal: true

class NewTicketsNotifyWorker
  include Sidekiq::Worker

  def perform
    SavedFilter.already_confirmed.each_with_index do |filter, _index|
      flights = filter.flights.order(created_at: :desc).includes(:city_to).includes(:city_from).limit(200)
      next if flights.empty?

      send_notifications(flights_text(flights), filter)
      flights.destroy_all
    end
  end

  def send_notifications(email_text, filter)
    SubscriptionMailer
      .with(email: filter.email, filter: filter, email_text: email_text)
      .new_flights
      .deliver_later
    # .deliver_now!
  end

  private

  def flights_text(flights_ar)
    flights = {}
    content = []

    flights_ar.each do |flight|
      flights[flight.city_to.title] = flights[flight.city_to.title] || []
      flights[flight.city_to.title] << flight.email_row
    end

    flights.each_key do |key|
      # content << "<img src='https://source.unsplash.com/400x130/?budapest' />"
      content << "<h3>#{key}</h3>"
      content << "<table><tbody>#{flights[key].join('')}</tbody></table>"
    end

    content.join('')
  end
end
