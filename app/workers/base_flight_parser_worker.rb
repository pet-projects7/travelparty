# frozen_string_literal: true

class BaseFlightParserWorker
  def flight_fine?(flights)
    flights.select do |flight|
      flight[:cost].to_i > 0
    end
  end
end
