# frozen_string_literal: true

class LatestFlightsWorker < BaseFlightParserWorker
  include Sidekiq::Worker

  def perform(cities)
    flights = get_flights(cities['city_from_code'], cities['city_to_code'], cities['currency']) || []

    unless flights.empty?
      flights = flights.select { |flight| flight['actual'] == true }

      creation_arr = flights.map do |flight|
        {
          source: :latest,
          city_from_id: cities['city_from_id'],
          city_to_id: cities['city_to_id'],
          date_from: flight['depart_date'],
          date_to: flight['return_date'],
          cost: flight['value'],
          currency: cities['currency'].to_sym,
          number_of_changes: flight['number_of_changes'],
          gate: flight['number_of_changes']
        }
      end

      creation_arr = flight_fine?(creation_arr)

      created_flights = create_flights(creation_arr)

      unless created_flights.ids.empty?
        created_flights.ids.each { |flight_id| Flight.find(flight_id).find_relevant_subscriptions }
      end
    end
  end

  private

  def get_flights(departure, destination, currency)
    FlightSearchService.latest(departure, destination, currency)
  end

  def create_flights(flights)
    Flight.import flights, on_duplicate_key_ignore: true

    # Flight.create(
    #   source: :latest,
    #   city_from: City.find_by(code: city_from),
    #   city_to: City.find_by(code: flight['destination']),
    #   date_from: flight['depart_date'],
    #   date_to: flight['return_date'],
    #   cost: flight['value'],
    #   currency: currency,
    #   number_of_changes: flight['number_of_changes']
    # )
  rescue ActiveRecord::RecordNotUnique => error
    true
  end
end
