import { Controller } from "stimulus"
import 'air-datepicker/dist/js/datepicker.min.js';
import 'air-datepicker/dist/css/datepicker.min.css';
require('selectize/dist/js/standalone/selectize.min.js');
require('selectize/dist/css/selectize.default.css');

// import Vue from 'vue'
// import App from 'components/app.vue'

export default class extends Controller {
  static targets = [ "output", 'flight_date_from', 'hotel_country' ]

  // connect() {
  //   const el = this.element

  //   const app = new Vue({
  //     el,
  //     render: h => h(App)
  //   })
  // }

  connect () {
    $(this.flight_date_fromTarget).datepicker()

    // $(this.hotel_countryTarget).selectize({
    //   delimiter: ',',
    //   persist: false,
    //   create: function(input) {
    //       return {
    //           value: input,
    //           text: input
    //       }
    //   }
    // });
  }
}
