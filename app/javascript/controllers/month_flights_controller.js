import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ ]

  connect () {
    this.flights = JSON.parse(this.data.get('flights'))

    this.renderFlights()
  }

  renderFlights () {
    const content = this.flights
      .sort(this.sortByDate)
      .map(flight => {
        return  `
          <div>
            <ul>
              <li> Возвращение: ${ flight['return_date'] }</li>
              <li> Вылет: ${ flight['depart_date'] }</li>
              <li> Пересадки: ${ flight['number_of_changes'] }</li>
              <li> Назначение: ${ flight['destination'] }</li>
              <li> Источник: ${ flight['gate'] }</li>
              <li> Цена: ${ flight['value'] }</li>
            </ul>
          </div>
        `
      })
      .join('')
      
      this.element.innerHTML = content
  }

  sortByDate (flight1, flight2) {
    return flight1.depart_date > flight2.depart_date
  }

  sortByPrice (flight1, flight2) {
    return flight1.price > flight2.price
  }
}
