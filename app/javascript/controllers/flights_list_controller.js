import { Controller } from "stimulus"
import dayjs from 'dayjs'
import 'dayjs/locale/ru'

const Skycons = require('skycons')

export default class extends Controller {
  getWeather (button) {

    const params = { filter: button.currentTarget.dataset}

    const weathrerForecast = $.getJSON('/weather_forecasts/get_weather_forecast', params).then((response) => {
      const content = this.forecastHTML(response).join('')
      const el = this.forecastContainer(content)

      var skycons = new(new Skycons({"color": "pink"}))

      el.querySelectorAll('canvas').forEach((canvasContainer) => {
        skycons.add(canvasContainer, canvasContainer.dataset.icon)
      })

      skycons.play();

      this.popup.showDOMElement(el)
    })
  }

  forecastHTML (weatherForecast) {
    return weatherForecast.map((dailyForecast) => {
      const icon = dailyForecast.additional_data.icon

      return `<div class='column weatherForecastBlock'>
                <div class='weatherForecastBlock__date'>${ dayjs(dailyForecast.day).locale('ru').format('DD MMM') }</div>
                <canvas class='weatherForecastBlock__icon' data-icon='${ icon }'></canvas>
                <div class='weatherForecastBlock__temperature'>Средняя температура: ${ (dailyForecast.min_temperature + dailyForecast.max_temperature) / 2 } &#8451;</div>
              </div>`
    })
  }

  forecastContainer (content) {
    var el = document.createElement('div');
    el.className = 'columns is-multiline weatherForecastBlock__container'
    el.innerHTML = content

    return el
  }

  get popup () {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('[data-controller="popup"]'), 'popup')
  }
}

// 0 "icon"
// 1 "time"
// 2 "summary"
// 3 "uvIndex"
// 4 "dewPoint"
// 5 "humidity"
// 6 "pressure"
// 7 "moonPhase"
// 8 "windSpeed"
// 9 "cloudCover"
// 10 "precipType"
// 11 "sunsetTime"
// 12 "sunriseTime"
// 13 "uvIndexTime"
// 14 "windBearing"
// 15 "pressureError"
// 16 "temperatureLow"
// 17 "temperatureMax"
// 18 "temperatureMin"
// 19 "windSpeedError"
// 20 "cloudCoverError"
// 21 "precipIntensity"
// 22 "temperatureHigh"
// 23 "windBearingError"
// 24 "precipProbability"
// 25 "precipIntensityMax"
// 26 "temperatureLowTime"
// 27 "temperatureMaxTime"
// 28 "temperatureMinTime"
// 29 "temperatureHighTime"
// 30 "temperatureLowError"
// 31 "temperatureMaxError"
// 32 "temperatureMinError"
// 33 "precipIntensityError"
// 34 "temperatureHighError"
// 35 "apparentTemperatureLow"
// 36 "apparentTemperatureMax"
// 37 "apparentTemperatureMin"
// 38 "precipIntensityMaxTime"
// 39 "apparentTemperatureHigh"
// 40 "precipIntensityMaxError"
// 41 "apparentTemperatureLowTime"
// 42 "apparentTemperatureMaxTime"
// 43 "apparentTemperatureMinTime"
// 44 "apparentTemperatureHighTime"
