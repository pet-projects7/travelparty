import { Controller } from "stimulus"

require('air-datepicker/dist/js/datepicker.min.js')
require('air-datepicker/dist/css/datepicker.min.css')

import { debounce } from 'lodash'

import URI from 'urijs';

export default class extends Controller {
  static targets = [ 'dateFrom', 'dateTo', 'form', 'flightsList', 'saveFilter', 'dateIn' ]

  initialize () {
    this.setDatePickers()
  }

  connect () {
    this.initEventListeners()
  }

  initEventListeners () {
    // window.addEventListener('scroll', _.debounce(this.scrollHandle.bind(this), 10))

    this.flightsListTarget.addEventListener('click', function (event) {
      if (event.target.dataset.selector == 'getWeatherButton') {
        App.weather.request_forecast(event.target.dataset)
      }

      if (event.target.dataset.selector == 'openAviasales') {
        yaCounter50607598.reachGoal('got_to_aviasales');
      }
    })

    this.formTarget.addEventListener('submit', function (event) {
      yaCounter50607598.reachGoal('filter');
    });

    this.saveFilterTarget.addEventListener('click', function (event) {
      const filterNames = ['city', 'date_from', 'date_to', 'duration', 'max_price']

      const formValues = this.formValuesAsJson(filterNames)

      var uri = new URI('/saved_filters/new').setSearch(formValues);

      Turbolinks.visit(uri.href())
    }.bind(this))
  }

  get saveFilterController () {
    const element = document.querySelector('[data-controller="filters-list"]')
    return this.application.getControllerForElementAndIdentifier(element, 'filters-list')
  }

  formValuesAsJson (fields) {
    let json = { };

    for (let [name, value] of new FormData(this.formTarget).entries()) {
      if (value.length > 0 && fields.indexOf(name) >= 0 ) {
        json[`saved_filter[${ name }]`] = value
      }
    }

    json['link'] = window.location.href
    json[`saved_filter[city_from_id]`] = this.data.get('cityFrom')

    return json
  }

  scrollHandle () {
    const top = window.scrollY
    const scrollFromTop = 452

    if (top > scrollFromTop && !this.formTarget.classList.contains('filterForm--fixed')) {
      this.formTarget.classList.toggle('filterForm--fixed', true)
    } else if (this.formTarget.classList.contains('filterForm--fixed') && top <= scrollFromTop) {
      this.formTarget.classList.toggle('filterForm--fixed', false)
    }
  }

  disconnect () {
    $(this.saveFilterTarget).off('click')
    this.arriveDatePicker.destroy()
    this.departureDatePicker.destroy()
  }

  setDatePickers () {
    // var startDate = new Date(2016, 10, 5);
    // var dp = $('#datepicker').datepicker({startDate: startDate}).data('datepicker');
    //
    // dp.selectDate(startDate)

    this.departureDatePicker = $(this.dateFromTarget).datepicker({
      autoClose: true
      // startDate: startDate
    }).data('datepicker')

    // this.departureDatePicker.selectDate(startDate)

    this.arriveDatePicker = $(this.dateToTarget).datepicker({
      autoClose: true
      // startDate: startDate
    }).data('datepicker')

    this.dateInPicker = $(this.dateInTarget).datepicker({
      autoClose: true,
      range: true
    }).data('datepicker')

    // this.arriveDatePicker.selectDate(startDate)
  }
}
