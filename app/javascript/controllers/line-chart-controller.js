import { Controller } from "stimulus"
import { extent, max, min } from "d3-array";
import { scaleLinear } from "d3-scale";
import { axisBottom, axisLeft } from 'd3-axis';
import { select } from "d3-selection";
import { line, curve, curveCatmullRom } from "d3-shape";

export default class extends Controller {

  config = {
    chartHeight: 200,
    bottomPadding: 20,
    url: this.data.get('url')
  }

  connect () {
    $
      .getJSON(this.config.url)
      .then(this.renderChart.bind(this))
  }

  renderChart (response) {
    this.getContainer()
    this.collectChartData(response)
    this.getScales()
    this.getAxis()
    this.renderAxis()
    this.renderBars()

    this.bottomLine()
  }

  getContainer () {
    this.container = select(this.element)
      .append('svg')
        .attr('height', this.config.chartHeight + this.config.bottomPadding)
        .attr('width', '100%')

    this.config.chartWidth = this.container.node().getBoundingClientRect().width
    this.config.barWidth = this.container.node().getBoundingClientRect().width / 31
  }

  collectChartData (response) {
    this.chartData = response
  }

  getScales () {
    const values = Object.values(this.chartData)
    this.max = max(values)
    this.min = min(values)

    this.verticalScale = scaleLinear()
      .domain([this.max, this.min])
      .range([0, this.config.chartHeight]);


    this.horizontalScale = scaleLinear()
      .domain([values.length, 0])
      .range([this.config.chartWidth, 0]);

  }

  getAxis () {
    this.axisLeft = axisLeft(this.verticalScale);
    this.axisBottom = axisBottom(this.horizontalScale);
  }

  renderAxis () {
    this.container
      .append('g')
      .style("transform", "translate(40px, 0px)")
      .call(this.axisLeft)

    // this.container
    //   .append('g')
    //   .style("transform", `translate(40px, ${ this.config.chartHeight }px)`)
    //   .call(this.axisBottom)
  }

  renderBars () {
    let barContainer = this.container
      .append('g')

    const chartData = this.chartData
    const vScale = this.verticalScale
    const config = this.config

    barContainer
      .selectAll('rect')
      .data(Object.keys(this.chartData))
      .enter()
      .append('rect')
      .attr('x', (d, i) => {
        return (this.config.barWidth * i) + 40
      })
      .attr('fill', 'grey')
      .attr('width', this.config.barWidth)
      .attr('height', (d, i) => {
        return vScale(chartData[d])
      })
      .attr('y', function (d, i) {
        return config.chartHeight - parseInt(this.getAttribute('height'))
      })
      .append('text')
      .text((d, i) => {
        return `${ d } - ${ this.chartData[d] }`
      })
      .attr('x', 0)
      .attr('y', 0)
      .style('color', 'white')
  }

  bottomLine () {
    const chartData = this.chartData
    const vScale = this.verticalScale
    const config = this.config

    let barContainer = this.container
      .append('g')
      .selectAll('text')
      .data(Object.keys(this.chartData))
      .enter()
      .append('text')
      .attr('x', (d, i) => {
        return (this.config.barWidth * i) + 40
      })
      .attr('y', config.chartHeight + config.bottomPadding)
      .attr('fill', 'grey')
      .attr('width', this.config.barWidth)
      .attr('height', 20)
      .style('font-size', '13px')
      .text((d) => {
        return d.split('-').slice(1,3).join('-')
      })
  }
}
