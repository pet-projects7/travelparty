import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ 'content' ]

  connect () {
    this.mainContentContainer = document.querySelector("[data-selector='mainContent']")
    this.bodyContainer = document.querySelector("body")

    document.addEventListener('keypress', this.closeOnEsc.bind(this))
  }

  disconnect() {
    document.removeEventListener('keypress', this.closeOnEsc.bind(this))
  }

  show (options = {}) {
    let { openCallback = () => {} } = options

    // openCallback()
    this.toggleCss(true)

  }

  close () {
    this.clearContent()
    this.toggleCss(false)
  }

  toggleCss (state) {
    this.element.classList.toggle('popup--shown', state)
    this.bodyContainer.classList.toggle('hiddenOverflow', state)
    this.mainContentContainer.classList.toggle('blured', state)
  }

  showContent (content) {
    this.contentTarget.innerHTML = content
    this.show()
  }

  showDOMElement (element) {
    this.contentTarget.appendChild(element)
    this.show()
  }

  clearContent () {
    this.contentTarget.innerHTML = ''
  }

  closeOnEsc (event) {
    if (event.charCode == 27) {
      this.close()
    }
  }
}
