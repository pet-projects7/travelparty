import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ 'trigger', 'content']

  connect () {
    self = this

    this.triggerTarget.addEventListener('click', this.openContent)
  }

  openContent () {
    var content = $(self.contentTarget);
    content.toggleClass('is-hidden')
    self.triggerTarget.innerHTML = self.triggerTarget.innerHTML == 'Открыть' ?  'Закрыть' : 'Открыть'
  }
}
