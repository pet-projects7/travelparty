import { Controller } from "stimulus"
import { groupBy, orderBy } from 'lodash'
const moment = require('moment')

export default class extends Controller {
  static targets = [ ]

  connect () {
    this.flights = JSON.parse(this.data.get('flights'))

    let grouppedByCities = _.orderBy(this.flights, 'city_to.title')
    grouppedByCities = _.groupBy(grouppedByCities, 'city_to.title')

    this.element.innerHTML = Object.keys(grouppedByCities).map((key) =>{
      return this.renderCityBlock(key, grouppedByCities)
    }).join('')
  }

  renderCityBlock (city_id, grouppedByCities) {
    return `<div class='flightItem'>
      <h3>${ city_id }</h3>

      <div>
        ${ this.renderCityPrices(grouppedByCities[city_id]) }
      </div>
    </div>`
  }

  renderCityPrices (prices) {
    return prices.map((price) => {
      // return `
      //   <div  class='flightItem__row'>
      //     Добавлен: ${ moment(price.created_at).format('L')  } 
      //     Вылет: ${ price.date_from } 
      //     Возвращение: ${ price.date_to } 
      //     Цена: ${ price.cost }
      //     Авиакомпания: ${ price.airline }
      //     <a href='${ this.aviasalesLink(price) }' target='_blank'>Подробнее</a>
      //   </div>
      // `
      return `
        <div  class='flightItem__row'>
          Д: ${ moment(price.created_at).format('L')  } 
          В: ${ price.date_from } 
          В: ${ price.date_to } 
          Ц: ${ price.cost }
          А: ${ price.airline }
          <a href='${ this.aviasalesLink(price) }' target='_blank'>Подробнее</a>
        </div>
      `
    }).join('')
  }

  aviasalesLink (price) {
    return `https://hydra.aviasales.ru/searches/new
?marker=182694
&origin_iata=${ price.city_from.code }
&destination_iata=${ price.city_to.code }
&depart_date=${ price.date_from }
&return_date=${ price.date_to }
&with_request=true`
  }
}
