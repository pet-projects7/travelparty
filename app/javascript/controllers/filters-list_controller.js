import { Controller } from "stimulus"
import URI from "urijs";

export default class extends Controller {
  static targets = [ 'filtersList']

  connect() {
    // this.drawFilter()
  }

  saveFilter (filterValues) {
    this.filter = this.filter.concat(filterValues)
  }

  drawFilter () {
    const listElement = this.filtersListTarget
    let htmlString = ''

    for (let val of this.filter) {
      htmlString += `<div class="savedFilter flex">${ this.filterTitle(val) }</div>`
    }

    listElement.innerHTML = htmlString
  }

  filterTitle (filter) {
    let title = []

    for(let key in filter) {
      if (key == 'link') { continue }
      title.push(`<div href='${ filter[key].link }'>${ this.translateFilterName(key) } - ${ filter[key] }</div>`)
    }

    title.push("<div class='buttons'>")
      title.push(`<button data-params='${JSON.stringify(filter)}' data-action="click->filters-list#subscribe" class='button'>Подписаться</button>`)
      title.push(`<input type='text' class="input" />`)
      title.push(`<button class='button' data-action="click->filters-list#delete">Удалить</button>`)
    title.push("</div>")

    // title += `<div class='buttons'><button data-params='${ JSON.stringify(filter) }' data-action="click->filters-list#subscribe" class='button'>Подписаться</button>`
    // title += `<input type='text' />`
    // title += `<button class='button' data-action="click->filters-list#delete">Удалить</button>`
    // title += `</div>`

    return title.join('')
  }

  get filter () {
    return JSON.parse(localStorage.getItem('savedFilters')) || []
  }

  set filter (value) {
    localStorage.setItem('savedFilters', JSON.stringify(value))
    this.drawFilter()
  }

  translateFilterName (name) {
    const titles = {
      'departure_date_from':  'Дата отправления',
      'max_price': 'Максимальная цена'
    }

    return titles[name] || name
  }

  subscribe (event) {
    const email = event.target.nextSibling.value
    const params = JSON.parse(event.target.dataset.params)
    delete params['link']

    $.post('/saved_filters', { saved_filter: { email, params }}).then(function (response) {

    })
  }

  delete () {
    console.log('delete')
  }

}
