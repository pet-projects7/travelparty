import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"

import 'bulma/css/bulma.min.css'

const application = Application.start()
const context = require.context("controllers", true, /.js$/)
application.load(definitionsFromContext(context))

$(document).on('turbolinks:click', function() {
  NProgress.start();
});

$(document).on('turbolinks:render', function() {
  NProgress.done();
  NProgress.remove();
});
