# frozen_string_literal: true

class WeatherForecastChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'chat_1'
  end

  def request_forecast(message)
    WeatherForecastWorker.perform_async(message)
  end
end
