# frozen_string_literal: true

require 'net/http'

class WeatherService
  attr_reader :lat, :lng, :time

  def initialize(lat, lng, time)
    # self.lat = City.first.additional_data['coordinates']['lat']
    # self.lng = City.first.additional_data['coordinates']['lon']

    self.lat = lat
    self.lng = lng
    self.time = time
  end

  def request
    uri = URI(url)
    res = Net::HTTP.get(uri)

    results = JSON.parse(res)
  end

  private

  attr_writer :lat, :lng, :time

  def url
    "https://api.darksky.net/forecast/2cf0ff9f345ce72d818166185fe8b524/#{lat},#{lng},#{time}?exclude=minutely,hourly&units=si"
  end
end
