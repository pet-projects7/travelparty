# frozen_string_literal: true

require 'net/http'
require 'httparty'

class FlightSearchService
  TOKEN = '2486f747ba688fcbd442579cd2dae6e9'
  # Accept-Encoding: gzip, deflate.

  # Цены на авиабилеты
  # Возвращает список цен, найденных нашими пользователями за последние 48 часов, в соответствии с выставленными фильтрами.
  def self.latest(origin = 'MOW', destination = 'PRG', currency = 'rub')
    url = "http://api.travelpayouts.com/v2/prices/latest?origin=#{origin}&destination=#{destination}&limit=1000&currency=#{currency}"

    response = HTTParty.get(url,
                            headers: { 'X-Access-Token' => TOKEN })

    return [] unless response['data'].is_a?(Array)

    response['data']
  end

  def self.direct(origin = 'MOW', destination = 'PRG', _currency = 'rub')
    url = "http://api.travelpayouts.com/v1/prices/direct?origin=#{origin}&destination=#{destination}&currency=RUB"

    response = HTTParty.get(url, headers: { 'X-Access-Token' => TOKEN })

    return [] unless response['data'].present?

    response['data'][destination].values
  end

  def self.offers
    uri = URI("http://api.travelpayouts.com/v2/prices/special-offers?token=#{TOKEN}")
    res = Net::HTTP.get(uri)
    results = JSON.parse(res)['data']

    return [] unless results.is_a?(Array)

    results
  end

  def self.cheapest(origin = 'MOW', destination = 'PRG', currency = 'rub')
    uri = URI("http://api.travelpayouts.com/v1/prices/cheap?token=#{TOKEN}&origin=#{origin}&destination=#{destination}&currency=#{currency}")
    res = Net::HTTP.get(uri)
    results = JSON.parse(res)['data']
    flights = results[destination]&.values

    return [] unless flights.present?

    flights
  end

  def self.month(origin = 'MOW', destination = 'PRG')
    uri = URI("http://api.travelpayouts.com/v2/prices/month-matrix?token=#{TOKEN}&origin=#{origin}&destination=#{destination}")
    res = Net::HTTP.get(uri)
    results = JSON.parse(res)['data']

    return [] unless results.is_a?(Array)

    results
  end

  def self.popular_directions(origin = 'MOW', currency = 'rub')
    uri = URI("http://api.travelpayouts.com/v1/city-directions?token=#{TOKEN}&origin=#{origin}&currency=#{currency}")
    res = Net::HTTP.get(uri)
    results = JSON.parse(res)['data']

    return [] unless results.is_a?(Array)

    results
  end

  def self.nearest(origin = 'MOW', destination = 'PRG', _page = 1, currency = 'rub')
    url = "http://api.travelpayouts.com/v2/prices/nearest-places-matrix?origin=#{origin}&currency=#{currency}&flexibility=7&destination=#{destination}&limit=20"

    response = HTTParty.get(url,
                            headers: { 'X-Access-Token' => TOKEN })

    prices = response.dig('data', 'prices')

    return [] if prices.nil?

    prices
  end
end
