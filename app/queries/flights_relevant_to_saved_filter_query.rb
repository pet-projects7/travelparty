# frozen_string_literal: true

class FlightsRelevantToSavedFilterQuery
  attr_accessor :initial_scope

  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def call(saved_filter)
    scope = city_from(initial_scope, saved_filter.city_from_id)
    scope = city_to(initial_scope, saved_filter.city_to_id)
    scope = max_price(initial_scope, saved_filter.max_price)
  end

  def city_to(scoped, params)
    params ? scoped.where('city_to_id = ?', params) : scoped
  end

  def city_from(scoped, params)
    params ? scoped.where('city_from_id = ?', params) : scoped
  end

  def max_price(scoped, params)
    params ? scoped.where('cost <= ?', params) : scoped
  end
end
