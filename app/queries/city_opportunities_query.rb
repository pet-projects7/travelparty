# frozen_string_literal: true

class CityOpportunitiesQuery
  def call(relation, params)
    relation
      .paginate(page: params[:page])
      .order(created_at: :desc)
      .includes(:flight)
      .includes(flight: %i[city_from city_to])
  end
end
