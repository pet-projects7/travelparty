# frozen_string_literal: true

class SavedFilterQuery
  attr_accessor :initial_scope

  def initialize(initial_scope)
    # SavedFilter.already_confirmed
    @initial_scope = initial_scope
  end

  def call(flight)
    # [{"city_to"=>13829, "city_from"=>13289, "date_from"=>"2019-02-19", "date_to"=>"2019-02-21", "price"=>7727.0, "duration"=>2, "city_to_title"=>"Милан", "id"=>242469105}]
    # params = flight
    scoped = city(initial_scope, flight['city_to'])
    scoped = city_from(scoped, flight['city_from'])
    scoped = date_from(scoped, flight['date_from'])
    scoped = date_to(scoped, flight['date_to'])
    # scoped = duration(scoped, flight[:duration])
    scoped = max_price(scoped, flight['price'] || 0)

    scoped
  end

  private def city(scoped, query = nil)
    scoped.where('city_to_id = ? OR city_to_id IS ?', query, nil)
  end

  private def city_from(scoped, query = nil)
    scoped.where('city_from_id = ?', query)
  end

  private def date_from(scoped, query = nil)
    scoped.where('DATE(date_from) <= ? OR date_from IS ?', query, nil)
  end

  private def date_to(scoped, query = nil)
    scoped.where('DATE(date_to) >= ? OR date_to IS ?', query, nil)
  end

  private def date_in(scoped, query = nil)
    scoped.where('DATE(date_to) >= ? OR date_to IS ?', query, nil)
  end

  private def duration(scoped, query = nil)
    scoped.where('duration = ? OR duration IS ? OR duration = 0', query, nil)
  end

  private def max_price(scoped, query = 0)
    scoped.where('max_price >= ? OR max_price IS ?', query, nil)
  end
end
