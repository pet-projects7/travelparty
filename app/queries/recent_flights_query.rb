# frozen_string_literal: true

class RecentFlightsQuery
  attr_accessor :initial_scope

  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def call(params)
    default_relations(@initial_scope)
    order(@initial_scope, params)
    # scope = paginate(@initial_scope, params)
    arriving_city(@initial_scope, params)
    by_source(@initial_scope, params)
    departing_date(@initial_scope, params)
    arriving_date(@initial_scope, params)
    max_price(@initial_scope, params)

    @initial_scope
  end

  def default_relations(scope)
    scope.order(created_at: :desc)
    scope.includes(:city_from)
    scope.includes(:city_to)
  end

  def order(scope, params = nil)
    params[:order] ? scope.order_scope(params[:order] || nil, params[:direction]) : scope
  end

  def paginate(scope, params = nil)
    scope.paginate(page: params[:page], per_page: 100)
  end

  def arriving_city(scope, params = nil)
    params[:city] ? scope.by_arriving_city(params[:city] || nil) : scope
  end

  def by_source(scope, params = nil)
    params[:source] ? scope.by_source(params[:source] || nil) : scope
  end

  def departing_date(scope, params = nil)
    params[:departure_date_from] ? by_departing_date(params[:departure_date_from] || nil) : scope
  end

  def arriving_date(scope, params = nil)
    params[:arrive_date_from] ? by_departing_date(params[:arrive_date_from] || nil) : scope
  end

  def max_price(scope, params = nil)
    params[:max_price] ? scope.by_max_price(params[:max_price] || nil) : scope
  end
end
