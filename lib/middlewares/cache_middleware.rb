# frozen_string_literal: true

class CacheMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)

    return @app.call(env) if req.params['cacheless'].present?

    [
      200,
      { 'Content-Type' => 'application/json' },
      [$redis.get(Product::PROMOTIONS_CACHE_KEY) || '']
    ]
  end
end
