# frozen_string_literal: true

namespace :parse do
  desc 'TODO'

  task cheapest_flights: :environment do
    Destination.json_for_parse.each do |cities|
      CheapFlightsWorker.perform_async(cities)
    end
  end

  task latest_flights: :environment do
    Destination.json_for_parse.each do |cities|
      LatestFlightsWorker.perform_async(cities)
    end
  end

  task nearest_flights: :environment do
    Destination.json_for_parse.each do |cities|
      NearestFlightsWorker.perform_async(cities)
    end
  end

  task direct_flights: :environment do
    Destination.json_for_parse.each do |cities|
      DirectFlightsWorker.perform_async(cities)
    end
  end
end
