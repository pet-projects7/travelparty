# frozen_string_literal: true

namespace :cache do
  desc 'Обновление кэша'

  task warm: :environment do
    app = ActionDispatch::Integration::Session.new Rails.application

    destinations = Destination.includes(:city_to).pluck(:city_from_id, 'cities.code')

    warm_destinations(destinations)
    warm_destinations_with_monthes(destinations_with_monthes(destinations))
  end

  private

  def destinations_with_monthes(destinations)
    monthes = [Date.today, Date.today + 1.month, Date.today + 2.month, Date.today + 3.month].map { |date| [date.beginning_of_month.strftime('%d.%m.%Y'), date.end_of_month.strftime('%d.%m.%Y')] }

    result_arr = []

    monthes.each do |month|
      destinations.each do |destination|
        result_arr << {
          date_from: month[0],
          date_to: month[1],
          city_from_id: destination[0],
          city_to_code: destination[1]
        }
      end
    end

    result_arr
  end

  def warm_destinations(destinations)
    destinations.each do |destination|
      city_from_id, city_to_code = destination

      url = Rails.application.routes.url_helpers.flights_city_url(city_from_id, city: city_to_code, host: 'http://vityan.com')

      uri = URI(url)
      Net::HTTP.get(uri)
    end
  end

  def warm_destinations_with_monthes(destinations)
    destinations.each do |destination_with_monthes|
      url_params = {
        city: destination_with_monthes[:city_to_code],
        host: 'http://vityan.com',
        date_from: destination_with_monthes[:date_from],
        date_to: destination_with_monthes[:date_to]
      }

      url = Rails.application.routes.url_helpers.flights_city_url(city_from_id, url_params)

      uri = URI(url)
      Net::HTTP.get(uri)
    end
  end
end
