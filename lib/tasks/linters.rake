# frozen_string_literal: true

namespace :linters do
  warnings = {
    rubocop: 154,
    reek: 372
  }

  task :rubocop, :environment do
    results = JSON.parse `rubocop --format json`
    errors_count = results['summary']['offense_count']

    raise "Error in rubocop #{errors_count} > #{warnings[:rubocop]}" if results['summary']['offense_count'] > warnings[:rubocop]
  end

  task :reek, :environment do
    results = JSON.parse `reek --format json`

    raise "Error in reek #{results.count} > #{warnings[:reek]}" if results.count > warnings[:reek]
  end

  task :all, :environment do
    Rake::Task['linters:rubocop'].execute
    Rake::Task['linters:rubocop'].execute
  end
end
