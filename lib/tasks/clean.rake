# frozen_string_literal: true

namespace :clean do
  desc 'TODO'
  task flights: :environment do
    Flight.expired.delete_all
  end

  task subscription_flights: :environment do
    SubscriptionFlight.delete_all
  end
end
