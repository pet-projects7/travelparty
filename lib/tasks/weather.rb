# frozen_string_literal: true

namespace :weather do
  task weather: :environment do
    batch = Sidekiq::Batch.new success: :weather_finished

    batch.jobs do
      destinations_cities.each do |city|
        lat = city['additional_data']['coordinates']['lat']
        lng = city['additional_data']['coordinates']['lon']

        (Date.today..Date.today + 365.days).to_a.each do |date|
          CrawlWeatherForecastWorker.perform_async(city.id, lat, lng, date)
        end
      end
    end
  end

  def destinations_cities
    City.enabled_ariving_cities
  end
end
