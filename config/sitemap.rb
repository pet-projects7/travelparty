# frozen_string_literal: true

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://vityan.com'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  Destination.includes(:city_from, :city_to).pluck('cities.code', 'city_tos_destinations.code').each do |destination|
    return unless destination[0].present? && destination[1].present?

    add flights_to_selected_city_path(destination[0], destination[1]),
        priority: 0.7,
        changefreq: 'daily',
        host: 'http://vityan.com'
  end
end
