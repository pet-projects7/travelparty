# frozen_string_literal: true

ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
                                       access_key_id: Settings.SES.access_key,
                                       secret_access_key: Settings.SES.secret_key
