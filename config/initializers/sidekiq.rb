# frozen_string_literal: true

# Sidekiq::Logging.logger = nil
Sidekiq::Logging.logger.level = Logger::WARN

if ENV['selenium_remote_url'].present?
  Sidekiq.configure_server do |config|
    config.redis = { url: 'redis://redis:6379' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: 'redis://redis:6379' }
  end
end
