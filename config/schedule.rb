# frozen_string_literal: true

# every 5.minutes do
#   rake 'parse:cheapest_flights'
# end

every 10.minutes do
  rake 'parse:latest_flights'
  rake 'parse:nearest_flights'
  rake 'parse:direct_flights'
end

every 5.minutes do
  rake 'pghero:capture_query_stats'
  rake 'pghero:capture_space_stats'
  rake 'warm:cache'
end

every 1.days do
  rake 'clean:flights'
  rake 'clean:subscription_flights'
end

every 4.hours do
  runner 'NewTicketsNotifyWorker.perform_async'
end

every 1.day, at: '5:00 am' do
  rake '-s sitemap:refresh'
end

# every 1.month do
#   rake 'parse:weather'
# end
