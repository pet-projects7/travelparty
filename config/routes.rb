# frozen_string_literal: true

Rails.application.routes.draw do
  resources :saved_filters do
    get 'activate', on: :collection
    get 'deactivate', on: :collection
  end

  devise_for :users

  root to: 'home#index'

  # resource :weather, only: :index,
  #
  get 'weather_forecasts/get_weather_forecast',
      to: 'weather_forecasts#get_weather_forecast',
      as: :get_weather_forecast

  get 'flights/:id/anywhere',
      to: 'cities#anywhere',
      as: :flights_to_anywhere

  get 'flights/:city_from/:city_to',
      to: 'cities#flights_to_selected_city',
      as: :flights_to_selected_city

  resources :cities, only: %i[index show] do
    get :flights_statistics, on: :member
  end

  # Admin part
  get '/admin', to: 'admin#index', as: :admin_home

  scope :admin, module: :admin, as: :admin do
    root to: 'home#index'

    resources :cities do
      resources :opportunities
      resources :hotel_reservations
      resources :flights

      get 'destinations', on: :member, as: :cheapest
      get 'stats', on: :member, as: :stats
    end

    resources :countries
  end

  # Utilities
  require 'sidekiq/web'
  require "fastentry/engine"
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
    mount Fastentry::Engine, at: '/fastentry'
  end

  mount ActionCable.server => '/cable'

  authenticate :user, ->(user) { user.has_role? :admin } do
    mount PgHero::Engine, at: 'pghero'
  end
end
