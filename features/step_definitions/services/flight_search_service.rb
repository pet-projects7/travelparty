# frozen_string_literal: true

Допустим(/сервер запросил новые прямые билеты/) do
  VCR.use_cassette 'latest_flights' do
    @tickets = FlightSearchService.latest
  end
end

Тогда(/сервер в ответ получает новые билеты/) do
  assert(@tickets.present? && @tickets.count == 1000, true)
end
