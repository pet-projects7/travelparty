# frozen_string_literal: true

Допустим(/пользователь открыл страницу с билетами/) do
  visit flights_to_anywhere_url(City.first, host: 'localhost:3000')
end

Допустим(/для созданных городов создано несколько билетов/) do
  City.all.each do |city|
    5.times { city.departing_flights << create(:flight) }
  end
end

Тогда(/пользователь видит список билетов/) do
  page.assert_selector('[data-target="latest-flights-filter.flightsList"] tr', minimum: 5)
end

Допустим(/пользователь выбрал город прилёта/) do
  page.all('#city option')[1].select_option
  click_on 'Найти'
end

Тогда(/пользователь вижит только билеты в выбранный город/) do
  page.assert_selector('[data-target="latest-flights-filter.flightsList"] tr', maximum: 4)
end
