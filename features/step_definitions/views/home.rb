# frozen_string_literal: true

Допустим(/создано два города/) do
  Settings.enabled_departure_cities.keys.each do |title|
    create(:city,
           title: title,
           code: Settings.enabled_departure_cities[title].code,
           currency: Settings.enabled_departure_cities[title].currency)
  end
end

Когда(/открываю главную страницу/) do
  visit root_path
end

То(/вижу список городов/) do
  page.assert_selector('.column', minimum: 1)
end
