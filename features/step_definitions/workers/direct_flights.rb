# frozen_string_literal: true

require 'sidekiq/testing'
Sidekiq::Testing.fake!

Допустим(/создано два destination/) do
  Destination.create city_to: City.first, city_from: City.second
end

Допустим(/сервер запустил DirectFlightsWorker/) do
  cities = Destination.json_for_parse.first

  @flights = Flight.count

  VCR.use_cassette 'direct_flights', record: :new_episodes do
    DirectFlightsWorker.new.perform(cities)
  end
end

Тогда(/количество билетов должно увеличиться/) do
  assert Flight.count > @flights
end
