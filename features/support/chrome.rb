# frozen_string_literal: true

Cucumber::Rails::Database.javascript_strategy = :truncation

require 'selenium/webdriver'

Capybara.register_driver :selenium do |app|
  params = {
    browser: :chrome
  }

  Capybara::Selenium::Driver.new(app, params)
end

Capybara.register_driver :headless_chrome do |app|
  caps = Selenium::WebDriver::Remote::Capabilities.chrome(loggingPrefs: { browser: 'ALL' })
  opts = Selenium::WebDriver::Chrome::Options.new

  chrome_args = %w[--headless --window-size=1920,1080 --no-sandbox --disable-dev-shm-usage]
  chrome_args.each { |arg| opts.add_argument(arg) }
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: opts, desired_capabilities: caps)
end

Capybara.configure do |config|
  config.javascript_driver = :headless_chrome
end

Capybara.server_host = 'localhost'
Capybara.server_port = '3000'
