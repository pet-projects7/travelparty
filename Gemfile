# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'mini_racer', platforms: :ruby
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'turbolinks', '~> 5'

gem 'redis-namespace'
gem 'redis-rack-cache'
gem 'redis-rails'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem 'bootsnap', '>= 1.1.0', require: false
gem 'mini_magick', '~> 4.8'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capistrano-rails'
  gem 'capybara-screenshot'
  gem 'capybara-selenium'
  gem 'chromedriver-helper'
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'faker', git: 'https://github.com/stympy/faker.git', branch: 'master'
  gem 'reek'
  gem 'selenium-webdriver'
  gem 'simplecov'
  gem 'vcr'
  gem 'webmock'
end

group :development do
  gem 'bullet'
  gem 'heavens_door'
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'meta_request'
  gem 'pp_sql'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :production do
  gem 'lograge'
  gem 'logstash-event'
  gem 'logstash-logger'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'bulma-rails', '~> 0.7.1'
gem 'devise'
gem 'jquery-rails'
gem 'rails_db', '2.0.4'
gem 'rolify'
gem 'sidekiq'
gem 'sidekiq-batch'
gem 'sidekiq-throttler'
gem 'simple_form'
gem 'slim'
gem 'slim-rails'
gem 'svgeez'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'webpacker', '~> 3.5'

gem 'activerecord-import'
gem 'capistrano-db-tasks', require: false
gem 'capistrano-rails-db'
gem 'capistrano-rvm'
gem 'capistrano-sidekiq', github: 'seuros/capistrano-sidekiq'
gem 'capistrano-yarn'
gem 'capistrano3-puma', github: 'seuros/capistrano-puma'
gem 'config'
gem 'data_migrate'
gem 'jsonb_accessor', '~> 1.0.0'
gem 'meta-tags'
gem 'rails-i18n', '~> 5.1' # For 5.0.x, 5.1.x and 5.2.x
gem 'rollbar'
gem 'rubocop'
gem 'sshkit'
gem 'whenever', require: false
gem 'will_paginate', '~> 3.1.0'

gem 'actionpack-page_caching'
gem 'aws-ses', '~> 0.6.0', require: 'aws/ses'
gem 'groupdate'
gem 'httparty'
gem 'pg_query', '>= 0.9.0'
gem 'pghero'
gem 'rails-assets-bpopup', source: 'https://rails-assets.org'
gem 'rails-assets-nprogress', source: 'https://rails-assets.org'
gem 'sitemap_generator'
gem 'sparkpost_rails'
gem 'premailer-rails'
gem 'fastentry'
gem "aws-sdk-s3", require: false
