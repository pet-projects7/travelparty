# API #
1. https://www.travelpayouts.com/ru#nothing
2. http://www.aviasales.ru/API
3. https://vc.ru/32229-razbor-poletov-kakoy-servis-vybrat-dlya-poiska-aviabiletov-skyscanner-ili-aviasales
4. http://ibe.qtravelcloud.com/documentation/flights

http://api.travelpayouts.com/v2/prices/month-matrix?currency=rub&origin=LED&destination=HKT&show_to_affiliates=true&token=2486f747ba688fcbd442579cd2dae6e9


# Подготовка SVG спрайта #
```
svgeez build --with-svgo --source app/assets/images/svg_icons --destination app/assets/images/svg_icons.svg 
```

# Парсинг авиабилетов #
Должен быть включен sidekiq и redis

```
rake parse:cheapest_flights
rake parse:latest_flights
rake parse:nearest_flights
rake parse:direct_flights
```

# Доступ к monit и sidekiq #
- http://vityan.com/sidekiq 
- http://vityan.com:2812/ admin monit

# Основные сущности #
- City - город
- Country - страна
- Destiantion - точка вылета и прилёта 

#Описание API
- TravelPayouts - https://api.travelpayouts.com/


# CI
- Обновление Dockerfile на Gitlab
```
docker build -t registry.gitlab.com/rukomoynikov/travelparty . 
docker push registry.gitlab.com/rukomoynikov/travelparty

```

https://dev.to/zimski/the-complete-guide-to-setup-a-cicd-for-rails-5-on-gitlab-2f2d

https://www.spritecloud.com/2017/11/set-up-and-run-your-cucumber-and-selenium-web-tests-on-gitlab-ci-within-minutes/
