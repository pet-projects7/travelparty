# frozen_string_literal: true

FactoryBot.define do
  factory :city do
    title { Faker::Address.city }
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    code { Faker::Address.country_code_long }
    country { create(:country) }
  end
end
