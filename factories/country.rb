# frozen_string_literal: true

FactoryBot.define do
  factory :country do
    title { Faker::Address.country }
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    code { Faker::Address.country_code_long }
  end
end
