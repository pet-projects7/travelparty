# frozen_string_literal: true

FactoryBot.define do
  factory :flight do
    city_from { create(:city) }
    city_to { create(:city) }

    first_date = Faker::Date.between_except(1.year.ago, 1.year.from_now, Date.today)
    second_date = Faker::Date.between_except(1.month.ago, 1.year.from_now, first_date)
    date_from { first_date }
    date_to { second_date }

    source { Flight.sources.keys.sample(1).first.to_sym }
    cost { rand(7000...15_000) }
    number_of_changes { rand(0..2) }
  end
end
