# frozen_string_literal: true

class SubscriptionMailerPreview < ActionMailer::Preview
  def verify_subscription
    params = {
      token: 'afasfasf-safasfsag-asgasg',
      filter: { city: 'Mos', price: '6000' },
      description: SavedFilter.last.description
    }

    SubscriptionMailer.with(params).verify_subscription
  end

  def new_flights
    params = {
      email: 'cat@mos-it.com',
      filter: SavedFilter.last,
      email_text: flights_text
    }

    SubscriptionMailer.with(params).new_flights
  end

  def flights_text
    flights = {}
    content = []

    Flight.order(created_at: :desc).includes(:city_to).includes(:city_from).limit(200).each do |flight|
      flights[flight.city_to.title] = flights[flight.city_to.title] || []
      flights[flight.city_to.title] << flight.email_row
    end

    flights.each_key do |key|
      content << "<h3>#{key}</h3>"
      content << "<table><tbody>#{flights[key]}</tbody></table>"
    end

    content.join('')
  end
end
